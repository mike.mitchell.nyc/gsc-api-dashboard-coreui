const Combinatorics = require("js-combinatorics");

exports.removeStopWords = function(name) {
  // list of words to skip
  const stopWords = [
    "a",
    "an",
    "and",
    "are",
    "as",
    "at",
    "be",
    "by",
    "for",
    "from",
    "has",
    "he",
    "in",
    "is",
    "it",
    "its",
    "of",
    "on",
    "that",
    "the",
    "was",
    "were",
    "will",
    "with"
  ];
  let splitName = name.toLowerCase().split(" "); // this becomes the variable that holds the name without the stop words
  splitName.forEach(function(el, index) {
    // this removes the stopwords
    if (stopWords.indexOf(el) > -1) {
      let indexOfStopWord = index;
      let y = splitName.splice(indexOfStopWord, 1);
    }
  });
  let cmb = Combinatorics.permutationCombination(splitName);
  nameNoStopWords = cmb.toArray();
  return splitName;
};

// remove duplicates from an array
exports.removeDuplicates = function(arr) {
  return arr.reduce((x, y) => (x.includes(y) ? x : [...x, y]), []);
};

exports.addSentenceVariationsToAddressLists = function(
  address,
  index,
  a,
  addresses
) {
  if (addresses.length > 0) {
    addresses.forEach(function(item) {
      let itemArray = item.split(" ");
      for (let i = 0; i < a.length; i++) {
        let y = itemArray.splice(index, 1, a[i]);
        if (addresses.indexOf(itemArray) === -1) {
          addresses.push(itemArray.join(" "));
        } else {
          console.log(itemArray);
        }
      }
    });
  } else {
    for (let i = 0; i < a.length; i++) {
      let y = address.splice(index, 1, a[i]);
      addresses.push(address.join(" "));
    }
  }

  return addresses;
};

function addSentenceVariationsToAddressLists(address, index, a, addresses) {
  if (addresses.length > 0) {
    addresses.forEach(function(item) {
      let itemArray = item.split(" ");
      for (let i = 0; i < a.length; i++) {
        let y = itemArray.splice(index, 1, a[i]);
        if (addresses.indexOf(itemArray) === -1) {
          addresses.push(itemArray.join(" "));
        } else {
          console.log(itemArray);
        }
      }
    });
  } else {
    for (let i = 0; i < a.length; i++) {
      let y = address.splice(index, 1, a[i]);
      addresses.push(address.join(" "));
    }
  }

  return addresses;
}
