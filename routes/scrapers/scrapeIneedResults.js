const ineed = require("ineed");
const fs = require("fs");
const Nightmare = require("nightmare");
require("nightmare-download-manager")(Nightmare);
const nightmare = Nightmare({
  show: false,
  height: 900,
  width: 1400,
  ignoreDownloads: true
});
require("events").EventEmitter.prototype._maxListeners = 100;

const scrapeIneedResults = async (company, ineedResults) => {
  const links = ineedResults.map(result => result.href);
  console.log(links);
  // console.log(company);
  // { address:
  //       { address: '1501 N Dallas Ave',
  //         city: 'Lamesa',
  //         state: 'TX',
  //         zipcode: '79331',
  //         unit: '',
  //         addressVariations:
  //          [ '1501 north dallas ave',
  //            '1501 n dallas ave',
  //            '1501 north dallas av',
  //            '1501 north dallas aven',
  //            '1501 north dallas avenu',
  //            '1501 north dallas avenue',
  //            '1501 north dallas avn',
  //            '1501 north dallas avnue',
  //            '1501 n dallas av',
  //            '1501 n dallas aven',
  //            '1501 n dallas avenu',
  //            '1501 n dallas avenue',
  //            '1501 n dallas avn',
  //            '1501 n dallas avnue' ] },
  //      phoneNumber:
  //       { phoneNumber: '(806) 872-2884',
  //         onlyNumbersPhoneNumber: '8068722884',
  //         phoneAreaCode: '806',
  //         phonePrefix: '872',
  //         phoneLastFour: '2884' },
  //      name:
  //       { name: 'Pedroza\'s',
  //         nameNoStopWords: [ [Array] ],
  //         nameAsArrayWithNoStopWords: [ 'pedroza\'s' ],
  //         nameAsArray: [ 'pedroza\'s' ] },
  //      url: { url: 'No Url Found!', subdomain: null, getDomain: null } }

  let urlArray = [];
  await links
    .reduce(function(accumulator, url, currentIndex, array) {
      return accumulator.then(function(results) {
        return nightmare
          .downloadManager()
          .goto(url)
          .evaluate(() => document.body.innerHTML)
          .then(html => {
            urlArray.push(ineed.collect.texts.hyperlinks.fromHtml(html));
            console.log(
              "current url & index",
              url,
              currentIndex,
              "of",
              array.length
            );
          })
          .catch(err => console.log("error", err));
      });
    }, Promise.resolve([]))
    .then(function(results) {
      console.log("results", urlArray);
      return nightmare.end();
    })
    .catch(err => console.log("error", err, urlArray));

  //   await nightmare
  //     .goto("https://www.google.com/")
  //     .type(
  //       "#lst-ib",
  //       `${company.name.name} ${company.phoneNumber.phoneNumber} ${
  //         company.address.zipcode
  //       }`
  //     )
  //     .click('input[value="Google Search"]')
  //     .wait("html")
  //     // .evaluate(() => document.body.innerHTML)
  //     .url()
  //     .then(async function(url) {
  //       const newUrl = url.split("?").join("?num=100&");
  //       return nightmare
  //         .goto(newUrl)
  //         .wait("html")
  //         .evaluate(() => document.body.innerHTML)
  //         .end();
  //     })
  //     .then(async result => {
  //       return await ineed.collect.hyperlinks.fromHtml(result);
  //     })
  //     .then(hyperlinks =>
  //       hyperlinks.hyperlinks
  //         .filter(value => value.href.charAt(0) !== "/")
  //         .filter(
  //           value => value.href.indexOf("webcache.googleusercontent.com") === -1
  //         )
  //         .filter(value => value.href.indexOf("google.com") === -1)
  //         .filter(value => value.href.indexOf("javascript:void(0)") === -1)
  //         .filter(value => value.href !== "")
  //         .filter(value => value.href !== "https://www.youtube.com/?gl=US")
  //         .filter(value => value.href !== "https://www.blogger.com/?tab=wj")
  //         .filter(value => value.href.charAt(0) !== "#")
  //         .map(value => googleHarvestedUrls.push(value))
  //     )
  //     .catch(function(error) {
  //       console.error("Error:", error);
  //     });

  // const query = encodeURI(
  //   `https://www.google.com/search?q=${company.name.name}+${
  //     company.phoneNumber.phoneNumber
  //   }+${company.address.zipcode}`
  // );

  // nightmare
  //   .useragent(
  //     "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36"
  //   )
  //   .goto("https://www.google.com/")
  //   .type(
  //     "#lst-ib",
  //     `${company.name.name} ${company.phoneNumber.phoneNumber} ${
  //       company.address.zipcode
  //     }`
  //   )
  //   .click('input[value="Google Search"]')
  //   .wait("html")

  //   .evaluate(() => console.log("doc", document))
  //   .url(url => console.log("url", url))
  //   // .end()
  //   .end()
  //   .then(function(result) {
  //     console.log("result", result);
  //   })
  //   .catch(function(error) {
  //     console.error("Error:", error);
  //   });
};

module.exports = { scrapeIneedResults };
