const prom = require("nano-promises");
const nano = require("nano")(
  `http://${process.env.DB_USER}:${process.env.DB_PASS}@localhost:5984`
);
const axios = require("axios");
const clientSecrets = require("../gsc_advanced/client_secret.json");
const CLIENT_ID = clientSecrets.web.client_id;
const CLIENT_SECRET = clientSecrets.web.client_secret;
const REDIRECT_URL = clientSecrets.web.redirect_uris[0];
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const oauth2Client = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
const _ = require("lodash");

const filterItems = function(query, body) {
  return body.filter(function(el) {
    return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
  });
};

const allDBs = prom(nano);

const getTokenFromGoogleApi = async tokenObject =>
  await axios.get(
    `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${
      tokenObject.id_token
    }`
  );

const handleTokens = async (tokens, userDB, dbName, res) => {
  // get the token object
  const tokenObject = tokens.tokens;

  // if there is no refresh_token then they've registerd wtih us before using this email address
  // make sure to pass emails stored in state from frontend and pass through to /google-login to double guarantee it
  if (
    tokenObject.refresh_token === undefined &&
    tokenObject.access_token !== undefined
  ) {
    getTokenFromGoogleApi(tokenObject)
      .then(async currentKeysData => {
        let borrowedTokensObject = [];
        // check to see if email is in use
        const emailAttemptingToAuth = currentKeysData.data.email;
        const keysDoc = await userDB
          .get("keys")
          .then(keysDocumentResponse => {
            // get the keys document from the response
            const keysDocument = keysDocumentResponse[0];

            // get the keys array in the keys document
            const keysArray = keysDocument.keys;

            // if email exists then check to see if it is a duplicate
            if (keysArray.length > 0) {
              keysArray.forEach(async key => {
                if (
                  key.metadata.email &&
                  key.metadata.email === emailAttemptingToAuth
                ) {
                  res.redirect(
                    `http://localhost:3000/close?status=GoogleDuplicate`
                  );
                }
              });
            }

            // if it's not a duplicate, look through other DBs for the refresh_token
            allDBs.db.list(async function(err, body) {
              // filter items to only dbs that start with privatedb$ and return array
              const userDBs = filterItems("privatedb$", body);

              userDBs.forEach(async function(dbToCheckForExistingAuth) {
                // only look at DBs whch are not the users
                console.log("check", dbToCheckForExistingAuth, dbName);
                if (dbToCheckForExistingAuth !== dbName) {
                  console.log("success", dbToCheckForExistingAuth, dbName);
                  const dbToScan = allDBs.db.use(dbToCheckForExistingAuth);
                  const keys = await dbToScan
                    .get("keys")
                    // keys[0] is the return object
                    .then(otherKeysDocumentResponse => {
                      // get the json of the document
                      const otherKeysDocument =
                        otherKeysDocumentResponse[0].keys;

                      if (otherKeysDocument.length > 0) {
                        otherKeysDocument.map(key => {
                          if (key) {
                            const tokensArray = key.tokens;

                            const mostRecentToken =
                              tokensArray[tokensArray.length - 1] ||
                              tokensArray[0];

                            // set the credentials using the most recent token

                            if (mostRecentToken.refresh_token) {
                              oauth2Client.setCredentials({
                                access_token: mostRecentToken.access_token,
                                refresh_token: mostRecentToken.refresh_token,
                                expiry_date: mostRecentToken.expiry_date
                              });

                              // if the token has expired then get another one
                              if (mostRecentToken.expiry_date < Date.now()) {
                                // refresh access token
                                oauth2Client.refreshAccessToken(function(
                                  err,
                                  tokens
                                ) {
                                  if (err) {
                                    console.log(
                                      "err",
                                      err,
                                      "refresh access token err"
                                    );
                                  }

                                  // add tokens to the tokens array
                                  borrowedTokensObject.push(tokens);

                                  if (
                                    currentKeysData.data.azp ===
                                    currentKeysData.data.aud
                                  ) {
                                    //   console.log(currentKeysData.data);
                                    // create the new keys array to be added to the DB
                                    const d = new Date();
                                    const date = d.toUTCString();

                                    const newKeys = {
                                      metadata: currentKeysData.data,
                                      tokens: borrowedTokensObject,
                                      dateAdded: date
                                    };

                                    // add newKeys to the key array

                                    if (newKeys.tokens[0].refresh_token) {
                                      if (keysArray.length > 0) {
                                        keysArray.forEach(keyToCompare => {
                                          console.log(newKeys, keyToCompare);
                                          if (
                                            _.isEqual(newKeys, keyToCompare)
                                          ) {
                                            console.log(
                                              "keys are equal and duplicate"
                                            );
                                          } else {
                                            keysArray.push(newKeys);
                                            console.log(
                                              "keysDocument",
                                              JSON.stringify(keysDocument)
                                            );
                                            // add that into the keys db
                                            userDB
                                              .insert(keysDocument, "keys")
                                              .then(function([body, headers]) {
                                                console.log(
                                                  "successfully inserted tokens"
                                                );
                                              })
                                              .catch(function(err) {
                                                console.error(
                                                  "error inserting tokens: ",
                                                  err
                                                );
                                              });
                                          }
                                        });
                                      } else {
                                        keysArray.push(newKeys);
                                        console.log(
                                          "keysDocument",
                                          JSON.stringify(keysDocument)
                                        );
                                        // add that into the keys db
                                        userDB
                                          .insert(keysDocument, "keys")
                                          .then(function([body, headers]) {
                                            console.log(
                                              "successfully inserted tokens"
                                            );
                                          })
                                          .catch(function(err) {
                                            console.error(
                                              "error inserting tokens: ",
                                              err
                                            );
                                          });
                                      }
                                    }
                                  }
                                });
                              }
                            }
                          }
                        });
                      }
                    })
                    .catch(e => console.log("error getting keys", e));
                }
              });
            });
            // insert the new key into the database
            //   userDB
            //     .insert(keysDocument, "keys")
            //     .then(function([body, headers]) {
            //       res.redirect(
            //         `http://localhost:3000/close?status=GoogleSuccess`
            //       );
            //     })
            //     .catch(function(err) {
            //       console.error("error inserting keysDocument", err, keys);
            //     });

            console.log(
              "handle me later --> $flowfixme --> This check is necessary to prevent ID tokens issued to a malicious app being used to access data about the same user on your app's backend server "
            );
          })

          // make sure the google ids match for token authentication

          .catch(err => console.log(err, "it was a duplicate error, probably"));
      })
      .catch(err => console.log("axios error", err));
  }
};

const oauth2Callback = function(req, res, googleApisOauth2Client) {
  const dbName = req.query.state;

  //wrap nano db in promises
  const userDB = allDBs.db.use(dbName);

  // get all of the token data using req.query.code
  return googleApisOauth2Client
    .getToken(req.query.code)
    .then(tokens => handleTokens(tokens, userDB, dbName, res))
    .catch(function(err) {
      console.log("error", err, "oauth2error");
    });
};
module.exports = oauth2Callback;
