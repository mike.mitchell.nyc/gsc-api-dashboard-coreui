const axios = require("axios");

const getGooglePlaceId = function(req, res) {
  const placeId = req.body.placeId;
  axios
    .get(
      `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeId}&key=AIzaSyCmmJXyYOyrsTjtkuOy6mHc3kqfPD_0CNo`
    )
    .then(response => res.send(response.data.result))
    .catch(err => console.log("googlegetPlaceiderror", err));
};

module.exports = getGooglePlaceId;
