const googleLogin = function(req, res, googleApisOauth2Client) {
  const dbName = req.body.pathname.substring(1);
  var url = googleApisOauth2Client.generateAuthUrl({
    access_type: "offline",
    scope: "https://www.googleapis.com/auth/webmasters.readonly profile email",
    approval_prompt: "force",
    state: dbName
  });
  res.send(url);
};

module.exports = googleLogin;
