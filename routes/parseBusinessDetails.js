const parseCitations = require("../utils/parseCitations");
const tldjs = require("tldjs");
const addressObject = require("../citation_checker/usps-codes.json");
const Combinatorics = require("js-combinatorics");
const { spawnMyScrapers } = require("./spawnMyScrapers");

const parseBusinessDetails = function(req, res) {
  // console.log(req.body);
  const {
    businessName,
    addressLineOne,
    addressLineTwo,
    city,
    state,
    zipcode,
    country,
    phone,
    website,
    businessCategories,
    openHours,
    shortestDescription,
    shortDescription,
    normalDescription,
    longDescription,
    periods,
    open_now,
    weekday_hours,
    dataFromPlaceId
  } = req.body;
  // console.log(dataFromPlaceId);


  let company = {},
    addresses = [],
    nameNoStopWords;

  const address = addressLineOne,
    unit = addressLineTwo,
    fullAddress = dataFromPlaceId.formatted_address,
    phoneNumber = phone,
    name = businessName;
  const addressSplit = address.toLowerCase().split(" ");
  // console.log(addressSplit);

  let removeStopWords = function(name) {
    // list of words to skip
    const stopWords = [
      "a",
      "an",
      "and",
      "are",
      "as",
      "at",
      "be",
      "by",
      "for",
      "from",
      "has",
      "he",
      "in",
      "is",
      "it",
      "its",
      "of",
      "on",
      "that",
      "the",
      "was",
      "were",
      "will",
      "with"
    ];
    let splitName = name.toLowerCase().split(" "); // this becomes the variable that holds the name without the stop words
    splitName.forEach(function(el, index) {
      // this removes the stopwords
      if (stopWords.indexOf(el) > -1) {
        let indexOfStopWord = index;
        let y = splitName.splice(indexOfStopWord, 1);
      }
    });
    let cmb = Combinatorics.permutationCombination(splitName);
    nameNoStopWords = cmb.toArray();
    return splitName;
  };

  // remove duplicates from an array
  function removeDuplicates(arr) {
    return arr.reduce((x, y) => (x.includes(y) ? x : [...x, y]), []);
  }

  function addSentenceVariationsToAddressLists(address, index, a, addresses) {
    if (addresses.length > 0) {
      addresses.forEach(function(item) {
        let itemArray = item.split(" ");
        for (let i = 0; i < a.length; i++) {
          let y = itemArray.splice(index, 1, a[i]);
          if (addresses.indexOf(itemArray) === -1) {
            addresses.push(itemArray.join(" "));
          } else {
            console.log(itemArray);
          }
        }
      });
    } else {
      for (let i = 0; i < a.length; i++) {
        let y = address.splice(index, 1, a[i]);
        addresses.push(address.join(" "));
      }
    }

    return addresses;
  }

  addressSplit.forEach(function(addEl, addIndex) {
    // look at each word to replace it, if possible
    for (let key in addressObject) {
      // check out the keys in the addressObject (i.e. w, ave, etc.)
      if (addressObject.hasOwnProperty(key)) {
        // make sure that the object is an object
        let addressObjectArrays = addressObject[key]; // set the values, which are arrays, to this variable
        addressObjectArrays = addressObjectArrays.map(v => v.toLowerCase()); // make sure all arrays are lowercase for matching
        if (addressObjectArrays.indexOf(addEl) > -1) {
          // if there is an index match then proceed, otherwise ignore
          let index = addIndex; // declare the index of the addressSplit where the match occurred
          addSentenceVariationsToAddressLists(
            addressSplit,
            index,
            addressObjectArrays,
            addresses
          );
        }
      }
    }
  });
  const addressesNoDuplicates = removeDuplicates(addresses);
  const onlyNumbersPhoneNumber = phoneNumber.replace(/[^0-9]+/g, "");
  const nameAsArrayWithNoStopWords = removeStopWords(businessName);
  // console.log(onlyNumbersPhoneNumber);
  company.address = {
    address: addressLineOne,
    city,
    state,
    zipcode,
    unit: unit,
    addressVariations: addressesNoDuplicates
  };
  company.phoneNumber = {
    phoneNumber,
    onlyNumbersPhoneNumber,
    phoneAreaCode: onlyNumbersPhoneNumber.substring(0, 3),
    phonePrefix: onlyNumbersPhoneNumber.substring(3, 6),
    phoneLastFour: onlyNumbersPhoneNumber.substring(6, 10)
  };
  company.name = {
    name,
    nameNoStopWords,
    nameAsArrayWithNoStopWords,
    nameAsArray: name.toLowerCase().split(" ")
  };
  company.url = {
    url: website,
    subdomain: tldjs.getSubdomain(website),
    getDomain: tldjs.getDomain(website)
  };

  // function to remove stop words

  removeStopWords(name);

  // console.log(JSON.stringify(company, null, 2));

  spawnMyScrapers("INITIAL_SCRAPE", company);
  res.send(company);
};

module.exports = parseBusinessDetails;
