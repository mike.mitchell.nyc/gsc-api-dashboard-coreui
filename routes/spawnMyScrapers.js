const { initialScraper } = require("./scrapers/initialScraper");

const spawnMyScrapers = (type, data) => {
  switch (type) {
    case "INITIAL_SCRAPE":
      initialScraper(data);
      break;
    default:
      break;
  }
};

module.exports = { spawnMyScrapers };
