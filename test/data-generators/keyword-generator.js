// Load Chance
const Chance = require("chance");
const { URL } = require("url");
const { performance } = require("perf_hooks");
const dotenv = require("dotenv").config({ path: "../../.env" });
const PouchDB = require("pouchdb");

const db = new PouchDB(`http://@localhost:5984/mdmitchell`, {
  ajax: {
    timeout: 60000
  },
  auth: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS
  }
});

// Instantiate Chance so it can be used
const chance = new Chance();

function getRandomInt(max, min = 1) {
  const numberToReturn = Math.floor(Math.random() * Math.floor(max));
  return numberToReturn < min ? getRandomInt(max) : numberToReturn;
}

const pqDataGenerator = (count = 1000) => {
  const url = new URL(chance.url());
  const origin = url.origin;
  const hostname = url.hostname;

  const date = new Date(chance.date({ year: 2018 }))
    .toISOString()
    .split("T")[0];

  const metadata = {
    _id: `pq_${origin}_${date}`,
    type: "pq",
    date: date,
    email: chance.email({ domain: hostname }),
    site: origin,
    modifiedRows: []
  };
  let data = { ...metadata };
  const urlGenerator = () => {
    return new URL(chance.url({ domain: hostname }));
  };
  function Datum() {
    this.clicks = chance.integer({ min: 0, max: 100000 });
    this.impressions = chance.integer({ min: 0, max: 10000000 });
    this.ctr = chance.integer({ min: 0, max: 1 });
    this.position = chance.integer({ min: 0, max: 1000 });
    this.keyword = chance.sentence({ words: getRandomInt(8) });
    this.url = urlGenerator();
    this.date = date;
  }

  [...Array(count)].map(() => {
    const newData = new Datum();
    newData.pathname = newData.url.pathname;
    newData.page = newData.url.href;
    delete newData.url;
    data.modifiedRows.push(newData);
  });

  return data;
};

var a = performance.now();

let promises = [];

[...Array(100)].forEach(() => {
  let docs = [];

  // ~ 7 seconds for every 100 at getRandomInt(5000)
  const generateArrays = () => {
    return [...Array(10)].map(() =>
      docs.push(pqDataGenerator(getRandomInt(5000)))
    );
  };
  generateArrays();

  promises.push(db.bulkDocs(docs).catch(e => console.log(e)));

  var b = performance.now();
  console.log("It took " + (b - a) / 1000 + " seconds");
});
// linearly increases ~2.7 seconds/100k records generated
Promise.all(promises)
  .then(function(result) {
    console.log("bulkdoc put success", result);
  })
  .catch(e => console.log("promise error", e));
