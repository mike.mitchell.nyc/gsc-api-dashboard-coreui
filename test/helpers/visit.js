const nightmare = require("nightmare");
const url = require("url");

const BASE_URL = url.format({
  protocol: process.env.PROTOCOL || "http",
  hostname: process.env.HOST || "localhost",
  port: process.env.PORT || 3000
});

const DEBUG = process.env.DEBUG || false;

module.exports = function(path = "", query = {}) {
  const location = url.resolve(BASE_URL, path);

  const page = nightmare({
    show: DEBUG,
    pollInterval: 50
  });

  return page.goto(location);
};
