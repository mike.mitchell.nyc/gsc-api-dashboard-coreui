const Nightmare = require("nightmare");
const assert = require("assert");
jest.setTimeout(30000);
const SuperLogin = require("superlogin");
const SLConfig = require("../../sl-config");
SLConfig.dbServer.user = "balls";
SLConfig.dbServer.password = "abluewhale";
const superlogin = new SuperLogin(SLConfig);

superlogin.on("user-db-removed", function(userDoc, provider) {
  console.log("User: " + userDoc._id + " logged in with " + provider);
});

const destroyDB = async credentials => {
  await superlogin
    .removeUser(credentials.username, true)
    .then(function() {
      console.log("User destroyed!");
    })
    .catch(err => console.log(err));
};

describe("It creates an account and then deletes it", function() {
  // Recommended: 5s locally, 10s to remote server, 30s from airplane ¯\_(ツ)_/¯

  const credentials = {
    username: "tester123abc",
    email: "arandomemail@emasadfasdfil.com",
    password: "asfdasfd!%!#%135135AFaa"
  };

  afterAll(async () => {
    return await destroyDB(credentials);
  });

  let nightmare = null;
  beforeEach(() => {
    nightmare = new Nightmare({ show: true, typeInterval: 1 });
  });

  test("The /registration process works", done => {
    let selector = "h1";
    // your actual testing urls will likely be `http://localhost:port/path`
    nightmare
      .goto("http://localhost:3000/register")
      .type("#username", credentials.username)
      .type("#email", credentials.email)
      .type("#password", credentials.password)
      .type("#confirmPassword", credentials.password)
      .type("#firstName", credentials.username)
      .type("#lastName", credentials.username)
      .type("#company", credentials.username)
      .click("#submitRegistration")
      .wait(3000)
      .evaluate(() => {
        return document;
      })
      .end()
      .then(function(result) {
        expect(result.location.href).toBe("http://localhost:3000/login");
        done();
      })
      .catch(done);
  });

  test("/login works", done => {
    let selector = "h1";
    // your actual testing urls will likely be `http://localhost:port/path`
    nightmare
      .goto("http://localhost:3000/login")
      .type("#username", credentials.username)
      .type("#password", credentials.password)
      .wait(1000)
      .click(".btn-success")
      .wait(3000)
      .evaluate(() => {
        return document;
      })
      .end()
      .then(function(result) {
        expect(result.location.href).toBe("http://localhost:3000/dashboard");
        done();
      })
      .catch(done);
  });
});
