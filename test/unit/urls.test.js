const visit = require("../helpers/visit");
const url = require("url");
jest.setTimeout(10000);

const BASE_URL = url.format({
  protocol: process.env.PROTOCOL || "http",
  hostname: process.env.HOST || "localhost",
  port: process.env.PORT || 3000
});

describe("Loading URLs", function() {
  test("it redirects / (Home Page) to /login", async function() {
    let page = visit("/");

    let text = await page
      .evaluate(() => document)
      .end()
      .then(result => {
        expect(result.location.href).toBe(`${BASE_URL}/login`);
      })
      .catch(console.log);
  });

  test("/login loads /login", async function() {
    let testUrl = "/login";
    let page = visit(testUrl);

    let text = await page
      .evaluate(() => document)
      .end()
      .then(result => expect(result.location.href).toBe(BASE_URL + testUrl))
      .catch(console.log);
  });

  test("/register loads /register", async function() {
    let testUrl = "/register";
    let page = visit(testUrl);

    let text = await page
      .evaluate(() => document)
      .end()
      .then(result => expect(result.location.href).toBe(BASE_URL + testUrl))
      .catch(console.log);
  });

  test("/forgot-password loads /forgot-password", async function() {
    let testUrl = "/forgot-password";
    let page = visit(testUrl);

    let text = await page
      .evaluate(() => document)
      .end()
      .then(result => expect(result.location.href).toBe(BASE_URL + testUrl))
      .catch(console.log);
  });

  test("/gobbledygooaksjdfasdfadf0135135!#%!#% loads /login", async function() {
    let testUrl = "/gobbledygooaksjdfasdfadf0135135!#%!#%";
    let page = visit(testUrl);

    let text = await page
      .evaluate(() => document)
      .end()
      .then(result => expect(result.location.href).toBe(BASE_URL + "/login"))
      .catch(console.log);
  });
});

// const Nightmare = require("nightmare");
// const assert = require("assert");
// jest.setTimeout(10000);

// describe("Load pages", function() {
//   // Recommended: 5s locally, 10s to remote server, 30s from airplane ¯\_(ツ)_/¯

//   let nightmare = null;
//   beforeEach(() => {
//     nightmare = new Nightmare();
//   });

//   test("/ (Home Page) redirects to /login", done => {
//     // your actual testing urls will likely be `http://localhost:port/path`
//     nightmare
//       .on(
//         "did-get-redirect-request",
//         (
//           event,
//           oldURL,
//           newURL,
//           isMainFrame,
//           httpResponseCode,
//           requestMethod,
//           referrer,
//           headers
//         ) => {
//           console.log(
//             event,
//             oldURL,
//             newURL,
//             isMainFrame,
//             httpResponseCode,
//             requestMethod,
//             referrer,
//             headers
//           );
//         }
//       )
//       .goto("http://localhost:3000/")
//       .evaluate(() => document)
//       .end()
//       .then(function(result) {
//         expect(result.location.href).toBe("http://localhost:3000/login");
//         done();
//       })
//       .catch(done);
//   });

//   test("/login page loads as expected", done => {
//     let selector = "h1";
//     // your actual testing urls will likely be `http://localhost:port/path`
//     nightmare
//       .goto("http://localhost:3000/login")
//       .evaluate(selector => {
//         // now we're executing inside the browser scope.
//         return document.querySelector(selector).innerText;
//       }, selector) // <-- that's how you pass parameters from Node scope to browser scope) => document.querySelector("h1"))
//       .end()
//       .then(function(result) {
//         console.log(result)
//         // expect(result.location.href).toBe("http://localhost:3000/login");
//         done();
//       })
//       .catch(done);
//   });
// });
