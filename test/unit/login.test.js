const Nightmare = require("nightmare");
const assert = require("assert");
jest.setTimeout(10000);

describe("Login page loads effectively", function() {
  // Recommended: 5s locally, 10s to remote server, 30s from airplane ¯\_(ツ)_/¯

  let nightmare = null;
  beforeEach(() => {
    nightmare = new Nightmare();
  });

  test("/login h1 loads", done => {
      
    let selector = "h1";
    // your actual testing urls will likely be `http://localhost:port/path`
    nightmare
      .goto("http://localhost:3000/login")
      .evaluate(selector => {
        // now we're executing inside the browser scope.
        return document.querySelector(selector).innerText;
      }, selector) // <-- that's how you pass parameters from Node scope to browser scope) => document.querySelector("h1"))
      .end()
      .then(function(result) {
        console.log(result)
        expect(result).toBe("Login to your account");
        done();
      })
      .catch(done);
  });
});

