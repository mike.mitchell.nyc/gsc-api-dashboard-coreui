// @flow

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const path = require("path");
const http = require("http");
const logger = require("morgan");
const SuperLogin = require("superlogin");
const dotenv = require("dotenv").config();
const cors = require("cors");
const SLConfig = require("./sl-config");
const clientSecrets = require("./gsc_advanced/client_secret.json");
const jwt = require("jwt-simple");
const Promise = require("bluebird");
const _ = require("lodash");
const prom = require("nano-promises");
const nano = require("nano");

const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const googleApisOauth2Client = new OAuth2(
  clientSecrets.web.client_id,
  clientSecrets.web.client_secret,
  clientSecrets.web.redirect_uris[0]
);
const port = process.env.PORT || 5000;
const superlogin = new SuperLogin(SLConfig);

const googleLogin = require("./routes/google-login");
const oauth2Callback = require("./routes/oauth2callback");
const getGooglePlaceId = require("./routes/getGooglePlaceId");
const parseBusinessDetails = require("./routes/parseBusinessDetails");

const asyncMiddleware = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next);
};

app.use(
  cors({ origin: ["http://localhost:3000", "https://www.googleapis.com/"] })
);

app.use(express.static(path.join(__dirname, "client", "build")));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "client", "build", "index.html"));
});

// Mount SuperLogin's routes to our app
app.use("/auth", superlogin.router);

app.post("/google-login", superlogin.requireAuth, function(req, res) {
  googleLogin(req, res, googleApisOauth2Client);
});

app.get("/oauth2callback", function(req, res) {
  oauth2Callback(req, res, googleApisOauth2Client);
});

app.post("/get-google-place-id", function(req, res) {
  getGooglePlaceId(req, res);
});

app.post("/parse-business-details", function(req, res) {
  parseBusinessDetails(req, res);
});

superlogin.onCreate(function(userDoc, provider) {
  console.log("created", userDoc);
  if (userDoc === undefined) {
    userDoc = {};
  }
  if (provider !== "local") {
    return (provider = "");
  }
  const nano = require("nano")(
    `http://${process.env.DB_USER}:${process.env.DB_PASS}@localhost:5984`
  );

  Promise.resolve(userDoc).then(userDoc => {
    const userDB = nano.db.use(Object.keys(userDoc.personalDBs)[0]);

    const userMetdata = {
      _id: "metadata",
      firstName: userDoc.firstName,
      lastName: userDoc.lastName,
      company: userDoc.company ? userDoc.company : "",
      email: userDoc.email, // userDoc.unverifiedEmail.email
      username: userDoc._id,
      signUpDate: userDoc.signUp.timestamp,
      personalDBs: Object.keys(userDoc.personalDBs)
    };

    const keys = {
      _id: "keys",
      keys: [],
      sites: []
    };

    const gscErrors = {
      _id: "gscErrors",
      noData: []
    };

    userDB.bulk({ docs: [userMetdata, keys, gscErrors] }, function(
      err,
      body,
      header
    ) {
      if (err) {
        console.log("[userDB.insert[userMetdata]] ", err.message);
        return;
      }
      console.log("you have inserted the metadata. Body response:");
      console.log(body);
      console.log("\n and headers: ", header);
    });
  });
});

app.listen(port, () => console.log(`Listening on port ${port}`));
