// @flow

// react
import React, { Component } from "react";

// react router
import { Switch, Route, Redirect } from "react-router-dom";

// types
import { Metadata, Props } from "../../SharedTypes";

// css
import { Container } from "reactstrap";

// lodash
import * as _ from "lodash";

// components for routes
import Header from "../../components/Header/";
import Sidebar from "../../components/Sidebar/";
import Breadcrumb from "../../components/Breadcrumb/";
import Aside from "../../components/Aside/";
import Footer from "../../components/Footer/";
import Dashboard from "../../views/Dashboard/";
import Widgets from "../../views/Widgets/";
import Settings from "../../views/Settings/";
import CitationManager from "../../views/CitationManager/CitationManager";
import GSCAdvanced from "../../views/GSCAdvanced/GSCAdvanced";

// pouchdb
import PouchDB from "pouchdb";

// superlogin
import superlogin from "superlogin-client";
import { SuperLoginClientConfig } from "../../sl-client-config";

superlogin.configure(SuperLoginClientConfig);

type State = {
  isLoading: boolean,
  authorized: boolean,
  localDB: string,
  remoteDB: string,
  metadata: Metadata
};

class Full extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      isLoading: true,
      authorized: false,
      localDB: "",
      remoteDB: "",
      metadata: {}
    };
  }

  async createOrSyncPouchDB(metadata: Metadata) {
    const remoteDB = await new PouchDB(metadata.userDBs.privatedb);
    const localDB = await new PouchDB(metadata.user_id);

    this.setState({
      remoteDB: metadata.userDBs.privatedb,
      localDB: metadata.user_id
    });

    try {
      const remoteDBChangesLength = await remoteDB
        .changes({ since: 0 })
        .then(changes => changes.results.length)
        .catch(console.log);

      const localDBChangesLength = await localDB
        .changes({ since: 0 })
        .then(changes => changes.results.length)
        .catch(console.log);

      if (localDBChangesLength !== remoteDBChangesLength) console.log("sync");
      try {
        remoteDB
          .sync(localDB)
          .on("complete", async function() {
            // console.log(localDBChangesLength);
          })
          .on("error", function(err) {
            console.log("sync not working", err);
            // this.props.history.push("/login");
          });
      } catch (err) {
        if (err.status === 404) {
          this.props.history.push("/login");
        }
      }
    } catch (err) {
      if (err.status === 404) {
        try {
          remoteDB.replicate
            .to(localDB, {
              filter: function(doc) {
                return doc._id === "metadata";
              }
            })
            .on("complete", async function() {
              console.log("sync complete");
            })
            .on("error", function(err) {
              console.log("sync not working", err);
              // this.props.history.push("/login");
            });
        } catch (err) {
          if (err.status === 404) {
            this.props.history.push("/login");
          }
        }
      }
    }

    const remoteMetadata = await remoteDB.get("metadata");
    const localMetadata = await localDB.get("metadata");

    if (_.isEqual(localMetadata, remoteMetadata)) {
      this.setState({
        isLoading: false,
        metadata: localMetadata,
        authorized: true
      });
    } else {
      remoteDB.sync(localDB);
      if (_.isEqual(localMetadata, remoteMetadata)) {
        this.setState(
          {
            isLoading: false,
            metadata: localMetadata,
            authorized: true
          },
          () => {
            remoteDB
              .close()
              .then(console.log("remote db closed"))
              .catch(console.log("remoteDB close error"));
            localDB
              .close()
              .then(console.log("local db closed"))
              .catch(console.log("local DB close error"));
          }
        );
      } else {
        this.props.history.push("/login");
      }
    }
  }

  // need to prevent component from loading
  async componentWillMount() {
    const metadata = await superlogin.getSession();
    if (!metadata) {
      this.props.history.push("/login");
    } else {
      const now = new Date();
      if (now > metadata.expires) {
        superlogin.deleteSession();
        this.props.history.push("/login");
      }

      superlogin
        .authenticate()
        .then(metadata => this.createOrSyncPouchDB(metadata))
        .catch(err => {
          console.log("error: ", err);
          this.props.history.push("/login");
        });
    }
  }

  render() {
    return (
      <div>
        {this.state.isLoading && <div>Loading...</div>}

        {!this.state.isLoading &&
          this.state.authorized && (
            <div className="app">
              <Header />
              <div className="app-body">
                <Sidebar {...this.props} />
                <main className="main">
                  <Breadcrumb />
                  <Container fluid>
                    <Switch>
                      <Route
                        path="/dashboard"
                        name="Dashboard"
                        component={props => (
                          <Dashboard {...props} someProp="2" />
                        )}
                      />
                      <Route
                        path="/settings"
                        name="Settings"
                        component={props => {
                          return (
                            <Settings
                              {...props}
                              localDB={this.state.localDB}
                              remoteDB={this.state.remoteDB}
                              metadata={this.state.metadata}
                            />
                          );
                        }}
                      />
                      <Route
                        path="/citation-manager"
                        name="Citation Manager"
                        component={props => {
                          return (
                            <CitationManager
                              {...props}
                              localDB={this.state.localDB}
                              remoteDB={this.state.remoteDB}
                              metadata={this.state.metadata}
                            />
                          );
                        }}
                      />
                      <Route
                        exact
                        path="/gsc-advanced"
                        name="GSCAdvanced"
                        component={props => {
                          return (
                            <GSCAdvanced
                              {...props}
                              localDB={this.state.localDB}
                              remoteDB={this.state.remoteDB}
                              metadata={this.state.metadata}
                            />
                          );
                        }}
                      />
                      {/*<Route path="/gsc-advanced/:keyword" component={Keyword}/>*/}
                      <Redirect from="/" to="/dashboard" />
                    </Switch>
                  </Container>
                </main>
                <Aside />
              </div>
              <Footer />
            </div>
          )}
      </div>
    );
  }
}

export default Full;
