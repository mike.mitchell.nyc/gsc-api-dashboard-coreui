
import { GoogleLogout, GoogleLogin } from "react-google-login";


const success = response => {
    console.log("success response", response);
};

const error = response => {
    console.error("error: ", response);
};

const loading = () => {
    console.log("loading");
};

const logout = () => {
    console.log("logout");
};


responseGoogle(response) {
    console.log("response generally", response);
    const script = document.createElement("script");
    document.body.appendChild(script);

    fetch(
        `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${
            response.tokenObj.id_token
        }`
    ).then(console.log);

    // script.src = "https://apis.google.com/js/client.js";
    // console.log(script, "script loaded");

    // script.onload = () => {
    //     gapi.load("client", {
    //         callback: () => {
    //             gapi.client.setToken(response.tokenObj.access_token);
    //             gapi.client
    //                 .load("webmasters", "v3", arg =>
    //                     console.log("undefined", arg)
    //                 )
    //                 .then(console.log("success?", success))
    //                 .catch(err => console.log("err", err));
    //         },
    //         onerror: function() {
    //             // Handle loading error.
    //             console.log("gapi.client failed to load!");
    //         },
    //         timeout: 5000, // 5 seconds.
    //         ontimeout: function() {
    //             // Handle timeout.
    //             console.log(
    //                 "gapi.client could not load in a timely manner!"
    //             );
    //         }
    //     });
    // };
}

//components 

<GoogleLogin
    clientId="463118519836-raq6tjoqatggkn5j82bv6d1u54nrcmkm.apps.googleusercontent.com"
    scope="https://www.googleapis.com/auth/webmasters.readonly"
    onSuccess={this.responseGoogle}
    onFailure={this.responseGoogle}
    onRequest={loading}
    responseType="code"
    offline={true}
    approvalPrompt="force"
    responseType="id_token"
    isSignedIn
    prompt="consent"
    className="btn btn-success"
    style={{}}
    buttonText="Add Google API Key"
/>



// original stuff
const responseGoogle = response => {
    
    const script = document.createElement("script");

    fetch(
        `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${
            response.tokenObj.id_token
        }`
    ).then(res => console.log(res, res.body));
    script.src = "https://apis.google.com/js/client.js";

    script.onload = () => {
        gapi.load("client", () => {

            gapi.client.setToken(response.tokenObj.access_token);
            gapi.client.load("webmasters", "v3", response => {
                console.log("this is the webmasters response", response);
                this.setState({ gapiReady: true });
            });
        });
    };
};


<GoogleLogin
clientId="463118519836-raq6tjoqatggkn5j82bv6d1u54nrcmkm.apps.googleusercontent.com"
scope="https://www.googleapis.com/auth/webmasters.readonly"
onSuccess={responseGoogle}
onFailure={responseGoogle}
onRequest={loading}
offline={false}
approvalPrompt="force"
responseType="id_token"
isSignedIn
// disabled
// prompt="consent"
// className='button'
// style={{ color: 'red' }}
>
<span>Analytics</span>
</GoogleLogin>

<GoogleLogout buttonText="Logout" onLogoutSuccess={logout} />