import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

// Styles
// Import Font Awesome Icons Set
import "font-awesome/css/font-awesome.min.css";
// Import Simple Line Icons Set
import "simple-line-icons/css/simple-line-icons.css";
import "react-bootstrap-typeahead/css/Typeahead.css";
import "react-table/react-table.css";
// Import Main styles for this application
import "../scss/style.scss";
// Temp fix for reactstrap
import "../scss/core/_dropdown-menu-right.scss";

ReactDOM.render(<App />, document.getElementById("root"));
