export type Metadata = {
    expires: number,
    ip: string,
    issued: number,
    password: string,
    provider: string,
    roles: string[],
    serverTimeDiff: number,
    token: string,
    userDBs: {
        privatedb: string
    },
    user_id: string
};

export type Props = {
    match: Object,
    location: Object,
    history: Object,
    staticContext: Object
};