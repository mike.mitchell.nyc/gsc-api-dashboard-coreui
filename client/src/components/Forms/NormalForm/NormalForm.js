import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Form,
  FormGroup,
  Row,
  Col,
  Card,
  CardGroup,
  CardBody,
  CardFooter,
  Button
} from "reactstrap";
import { NormalFormField } from "../../FormField/NormalFormField/NormalFormField";

class NormalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props.formState
    };

    this.handleUserInput = this.handleUserInput.bind(this);

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleUserInput(e) {
    // field name
    const name = e.target.name;

    // field value
    const value = e.target.value;

    // only validate if validation exists
    typeof this.props.inputValidation === "function"
      ? this.props.inputValidation(name, value)
      : "";

    this.setState({ [name]: value }, () => console.log(this.state));
  }

  handleFormSubmit(e) {
    e.persist();
    e.preventDefault();

    // console.log(this.state);

    typeof this.props.onFormSubmit === "function"
      ? this.props.onFormSubmit(name, value)
      : "";
  }

  render() {
    console.log(this.props.inputs);
    return (
      <Card>
        <Form onSubmit={this.handleFormSubmit}>
          <CardBody>
            <h1>{this.props.formInfo.formTitle}</h1>
            <p
              className={
                this.props.formInfo.formInstructionsCSS || "text-muted"
              }
            >
              {this.props.formInfo.formInstructions || ""}
            </p>

            {this.props.inputs.map((customProps, item) => (
              <FormGroup key={item} row>
                <NormalFormField
                  {...customProps}
                  value={this.state[customProps.id]}
                  onChangeValue={this.handleUserInput}
                />
              </FormGroup>
            ))}

            <Row>
              <Col md={{ size: 4, offset: 4 }} xs="12">
                <Button color="success" type="submit" value="Submit" block>
                  {this.props.formInfo.submitButtonText}
                </Button>
              </Col>
            </Row>
          </CardBody>
        </Form>
      </Card>
    );
  }
}

export default NormalForm;
