// add a business hours widget
// add a business description field < 50, < 100, <1000
// handle no open hours

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Form,
  FormGroup,
  Row,
  Col,
  Input,
  Card,
  CardGroup,
  CardBody,
  CardFooter,
  Button,
  Label
} from 'reactstrap';
import { InlineFormField } from '../../FormField/InlineFormField/InlineFormField';
import Geosuggest from 'react-geosuggest';
import _ from 'lodash';
import axios from 'axios';
import PouchDB from 'pouchdb';
import tldjs from 'tldjs';
import addressObject from './usps-codes.json';
import Combinatorics from 'js-combinatorics';

PouchDB.plugin(require('pouchdb-upsert'));

class InlineForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props.formState
    };

    this.handleUserInput = this.handleUserInput.bind(this);
    this.handleTimeInput = this.handleTimeInput.bind(this);

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.onSuggestSelect = this.onSuggestSelect.bind(this);
    this.toggleDateInputs = this.toggleDateInputs.bind(this);
  }

  async handleUserInput(e) {
    // field name
    const name = e.target.name;

    // field value
    const value =
      type === 'time'
        ? e.target.value.replace(/\:.*/, c => ':' + c.replace(/\:/g, () => ''))
        : e.target.value;

    //field type
    const type = e.target.type;

    // only validate if validation exists
    // typeof this.props.inputValidation === "function"
    //   ? this.props.inputValidation(name, value)
    //   : "";

    this.setState({ [name]: value }, () => console.log(this.state));
  }

  async handleTimeInput(e) {
    // field name
    const name = e.target.name;

    // field value
    const value = e.target.value;

    const index = name.charAt(0);
    const openOrClose = name.slice(1);

    let newPeriods = [...this.state.periods];
    newPeriods[index][openOrClose]['time'] = value;

    console.log('name', name, value, index, openOrClose, newPeriods);

    // only validate if validation exists
    // typeof this.props.inputValidation === "function"
    //   ? this.props.inputValidation(name, value)
    //   : "";

    this.setState({ periods: newPeriods }, () => console.log(this.state));
  }

  async handleFormSubmit(e) {
    e.persist();
    e.preventDefault();

    // console.log(this.state);
    const localDB = await new PouchDB(this.props.localDB);
    const remoteDB = await new PouchDB(this.props.remoteDB);

    const id = (
      'cc_' +
      this.state.businessName +
      this.state.phone.replace(/\D/g, '') +
      this.state.addressLineOne.replace(/[\W_]+/g, '')
    ).replace(/\W/g, '');
    // console.log("what", this.state.businessName);

    // let company = {},
    //   addresses = [],
    //   nameNoStopWords;

    // const address = this.state.addressLineOne,
    //   unit = this.state.addressLineTwo,
    //   fullAddress = this.state.dataFromPlaceId.formatted_address,
    //   phoneNumber = this.state.phone,
    //   name = this.state.businessName;
    // const addressSplit = address.toLowerCase().split(" ");
    const company = await axios
      .post(`http://localhost:5000/parse-business-details`, { ...this.state })
      .then(response => response.data)
      .catch(err => console.log(err));

    localDB
      .putIfNotExists(id, { ...this.state, search: company })
      .then(function(response) {
        localDB.sync(remoteDB);
      })
      .catch(function(err) {
        console.log(err, id, 'this already exists, homie!');
      });

    // localDB.sync(remoteDB);

    typeof this.props.onFormSubmit === 'function'
      ? this.props.onFormSubmit(name, value)
      : '';
  }

  toggleDateInputs() {
    this.setState({ dateInputsToggle: !this.state.dateInputsToggle });
  }

  async onSuggestSelect(suggest) {
    if (suggest !== undefined) {
      console.log(suggest);

      const description = _.get(suggest, 'description');
      const label = _.get(suggest, 'label');
      const location = _.get(suggest, 'location');
      const gmaps = _.get(suggest, 'gmaps');
      const placeId = _.get(suggest, 'placeId');

      // eslint-disable-line
      const address_components = _.get(gmaps, 'address_components');
      const formatted_address = _.get(gmaps, 'formatted_address');
      const geometry = _.get(gmaps, 'geometry');
      const types = _.get(gmaps, 'types');

      // add comments to me when you figure out what i do
      // address_components
      //   ? address_components.map(component => {
      //       const long_name = _.get(component, "long_name");
      //       const short_name = _.get(component, "short_name");
      //       const types = _.get(component, "types");

      //       // console.log(long_name, short_name, types);
      //     })
      //   : "";

      // const sortedTypes = handleTypes(types);

      function removeValuesFromArray(wordList, text) {
        return wordList.filter((wordToMatch, index) => {
          const wordMatches = text.filter(word => word === wordToMatch);
          return wordMatches.length > 0 ? false : true;
        });
      }

      let addressData = {};

      await axios
        .post(`http://localhost:5000/get-google-place-id`, { placeId })
        .then(function(response) {
          const dataFromPlaceId = _.get(response, 'data');
          let website = _.get(dataFromPlaceId, 'website');
          if (website === undefined) {
            website = 'No Url Found!';
          }

          const businessName = _.get(dataFromPlaceId, 'name');
          let phone = _.get(dataFromPlaceId, 'formatted_phone_number');

          if (phone === undefined) {
            phone = '';
          }

          const formatted_address = _.get(dataFromPlaceId, 'formatted_address');
          const addressNoCommas = formatted_address
            ? formatted_address.replace(/,/g, '').split(' ')
            : '';
          const addressNoCommasOrSpaces = formatted_address.split(', ');
          const checkPremise = types.map(type => {
            switch (type) {
              case 'premise':
                const wholeAddress = addressNoCommasOrSpaces.splice(0, 2);
                addressNoCommasOrSpaces.unshift(wholeAddress.join(' '));
                break;
              default:
                break;
            }
          });

          const weekday_text = _.get(
            dataFromPlaceId,
            ['opening_hours', 'weekday_text'],
            ''
          );

          const open_now = _.get(
            dataFromPlaceId,
            ['opening_hours', 'open_now'],
            ''
          );

          const ogPeriods = _.get(
            dataFromPlaceId,
            ['opening_hours', 'periods'],
            ''
          );

          const periods = Array.isArray(ogPeriods)
            ? ogPeriods.map(period => {
                period.close.time = period.close.time.replace(/(.{2})$/, ':$1');
                period.open.time = period.open.time.replace(/(.{2})$/, ':$1');
                console.log('in map period', period);
                return period;
              })
            : '';

          console.log('logging periods', periods);

          const country = addressNoCommas[addressNoCommas.length - 1];
          const zipcode = addressNoCommas[addressNoCommas.length - 2];
          const state = addressNoCommas[addressNoCommas.length - 3];
          const city =
            addressNoCommasOrSpaces.length < 5
              ? addressNoCommasOrSpaces[1]
              : addressNoCommasOrSpaces[2];
          const addressLineOne = addressNoCommasOrSpaces[0];
          const addressLineTwo =
            addressNoCommasOrSpaces.length < 5
              ? ''
              : addressNoCommasOrSpaces[1];
          const categories = _.get(dataFromPlaceId, 'types');
          const businessCategories = removeValuesFromArray(
            categories,
            notCategories
          ).join(', ');
          console.log(suggest);

          const url = _.get(dataFromPlaceId, 'url');

          const id = (
            'cc_' +
            businessName +
            phone.replace(/\D/g, '') +
            addressLineOne.replace(/[\W_]+/g, '')
          ).replace(/\W/g, '');

          addressData = {
            dataFromPlaceId: { ...response.data },
            addressLineOne,
            addressLineTwo,
            businessName,
            city,
            country,
            website,
            phone,
            state,
            zipcode,
            businessCategories,
            weekday_text,
            periods,
            open_now
          };
        })
        .catch(function(error) {
          console.log(error);
        });

      this.setState({ ...addressData }, () => console.log(this.state));
    }
  }

  render() {
    // console.log(this.props.businessInputs);

    return (
      <Card>
        <CardBody>
          <h1>{this.props.formInfo.formTitle}</h1>
          <p
            className={this.props.formInfo.formInstructionsCSS || 'text-muted'}
          >
            {this.props.formInfo.formInstructions || ''}
          </p>
          <Form onSubmit={this.handleFormSubmit} inline>
            <Col md={12}>
              <FormGroup className="mb-2 mr-sm-2 mt-2">
                <Col md={3}>
                  <Label for="geosuggest" className="text-left">
                    Get Address Information
                  </Label>
                </Col>
                <Col md={8}>
                  <Geosuggest
                    ref={el => (this._geoSuggest = el)}
                    placeholder="Search for business name and city to auto-fill out the form below"
                    location={new google.maps.LatLng(40.7127753, -74.0059728)}
                    radius="20"
                    id="geosuggest"
                    minLength={3}
                    onSuggestSelect={this.onSuggestSelect}
                    onSuggestNoResults={this.onSuggestNoResults}
                  />
                </Col>
              </FormGroup>
            </Col>
            <br />
            {this.props.businessInputs.map((customProps, index) => {
              // console.log("custom props", customProps);
              return (
                <InlineFormField
                  {...customProps}
                  value={this.state[customProps.id]}
                  onChangeValue={this.handleUserInput}
                  key={index + customProps.id}
                />
              );
            })}

            <Col md={12} className="w-100">
              <h3>Business Hours </h3>
            </Col>
            <br />
            <Col md={12}>
              <Button onClick={this.toggleDateInputs}>
                Add Business Hours Manually
              </Button>
            </Col>

            {this.state.periods &&
              this.state.periods.map((period, index) => {
                const timeOpen = _.get(period, ['close', 'time'], '    ')
                  .replace(/(.{2})$/, ':$1')
                  .replace(/\:.*/, c => ':' + c.replace(/\:/g, () => ''));
                const timeClose = _.get(period, ['open', 'time'], '    ')
                  .replace(/(.{2})$/, ':$1')
                  .replace(/\:.*/, c => ':' + c.replace(/\:/g, () => ''));
                const dayCloseNumber = _.get(period, ['close', 'day'], '');
                const dayOpenNumber = _.get(period, ['open', 'day'], '');
                let dayClose = '';
                let dayOpen = '';

                switch (dayOpenNumber) {
                  case 1:
                    dayOpen = 'Monday';
                    break;
                  case 2:
                    dayOpen = 'Tuesday';
                    break;
                  case 3:
                    dayOpen = 'Wednesday';
                    break;
                  case 4:
                    dayOpen = 'Thursday';
                    break;
                  case 5:
                    dayOpen = 'Friday';
                    break;
                  case 6:
                    dayOpen = 'Saturday';
                    break;
                  case 0:
                    dayOpen = 'Sunday';
                    break;
                  default:
                    break;
                }
                switch (dayCloseNumber) {
                  case 1:
                    dayClose = 'Monday';
                    break;
                  case 2:
                    dayClose = 'Tuesday';
                    break;
                  case 3:
                    dayClose = 'Wednesday';
                    break;
                  case 4:
                    dayClose = 'Thursday';
                    break;
                  case 5:
                    dayClose = 'Friday';
                    break;
                  case 6:
                    dayClose = 'Saturday';
                    break;
                  case 0:
                    dayClose = 'Sunday';
                    break;
                  default:
                    break;
                }

                // console.log(period);
                return (
                  <Col md={6} key={dayCloseNumber + index}>
                    <FormGroup className="mb-2 mr-sm-2 mt-2">
                      <Col md={4}>
                        <Label className="text-left">{dayOpen}</Label>
                      </Col>
                      <Col md={4}>
                        <Label>Open</Label>
                        <Input
                          type="time"
                          id={index + dayOpen}
                          name={index + 'open'}
                          value={this.state.periods[index].open.time}
                          onChange={this.handleTimeInput}
                        />
                      </Col>
                      <Col md={4}>
                        <Label>Closed</Label>
                        <Input
                          type="time"
                          id={index + dayOpen}
                          name={index + 'close'}
                          value={this.state.periods[index].close.time}
                          onChange={this.handleTimeInput}
                        />
                      </Col>
                    </FormGroup>
                  </Col>
                );
              })}

            {this.state.dateInputsToggle &&
              this.props.dateInputs.map((customProps, index) => {
                const idOpen = customProps.id + 'Open';
                const idClosed = customProps.id + 'Closed';
                return (
                  <Col md={6} key={index + customProps.id}>
                    <FormGroup className="mb-2 mr-sm-2 mt-2">
                      <Col md={4}>
                        <Label for={customProps.id} className="text-left">
                          {customProps.label.text}
                        </Label>
                      </Col>
                      <Col md={4}>
                        <Input
                          type={customProps.type}
                          id={idOpen}
                          className={`form-control w-100 ${
                            customProps.cssClass
                          }`}
                          placeholder={customProps.placeholder || ''}
                          name={idOpen}
                          value={this.state[idOpen]}
                          onChange={this.handleUserInput}
                          required={customProps.required}
                          pattern={customProps.pattern || '.*'}
                          title={customProps.title || ''}
                        />
                      </Col>
                      <Col md={4}>
                        <Input
                          type={customProps.type}
                          id={idClosed}
                          className={`form-control w-100 ${
                            customProps.cssClass
                          }`}
                          placeholder={customProps.placeholder || ''}
                          name={idClosed}
                          value={this.state[idClosed]}
                          onChange={this.handleUserInput}
                          required={customProps.required}
                          pattern={customProps.pattern || '.*'}
                          title={customProps.title || ''}
                        />
                      </Col>
                    </FormGroup>
                  </Col>
                );
              })}

            <br />
            <hr />
            <Row>
              <Col xs={12}>
                <h3>Business Descriptions </h3>
              </Col>
              <Col md={12}>
                <FormGroup className="mb-2 mr-sm-2 mt-2">
                  <Col md={3}>
                    <Label className="text-left">Shortest Description</Label>
                  </Col>
                  <Col md={9}>
                    <Input
                      type="text"
                      name="shortestDescription"
                      id="shortestDescription"
                      placeholder="50 characters or less"
                      width={'100%'}
                      value={this.state.shortestDescription}
                      className="w-100"
                      onChange={this.handleUserInput}
                    />
                  </Col>
                </FormGroup>
              </Col>
              <Col md={12}>
                <FormGroup className="mb-2 mr-sm-2 mt-2">
                  <Col md={3}>
                    <Label className="text-left">Short Description</Label>
                  </Col>
                  <Col md={9}>
                    <Input
                      type="text"
                      name="shortDescription"
                      id="shortDescription"
                      placeholder="100 characters or less"
                      value={this.state.shortDescription}
                      className="w-100"
                      onChange={this.handleUserInput}
                    />
                  </Col>
                </FormGroup>
              </Col>
              <Col md={12}>
                <FormGroup className="mb-2 mr-sm-2 mt-2">
                  <Col md={3}>
                    <Label className="text-left">Normal Description</Label>
                  </Col>
                  <Col md={9}>
                    <Input
                      type="textarea"
                      name="normalDescription"
                      id="normalDescription"
                      placeholder="250 characters or less"
                      value={this.state.normalDescription}
                      className="w-100"
                      rows="3"
                      onChange={this.handleUserInput}
                    />
                  </Col>
                </FormGroup>
              </Col>
              <Col md={12}>
                <FormGroup className="mb-2 mr-sm-2 mt-2">
                  <Col md={3}>
                    <Label className="text-left">Long Description</Label>
                  </Col>
                  <Col md={9}>
                    <Input
                      type="textarea"
                      name="longDescription"
                      id="longDescription"
                      placeholder="500 characters or less"
                      value={this.state.longDescription}
                      className="w-100"
                      rows="5"
                      onChange={this.handleUserInput}
                    />
                  </Col>
                </FormGroup>
              </Col>
              {
                //this.props.businessInputs.length % 2 === 1 ? <Col md={6} /> : ""
              }
              <Col md={{ size: 4, offset: 4 }} xs="12" className="mt-2">
                <Button color="success" type="submit" value="Submit" block>
                  {this.props.formInfo.submitButtonText}
                </Button>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
    );
  }
}

const notCategories = [
  'administrative_area_level_1',
  'administrative_area_level_2',
  'administrative_area_level_3',
  'administrative_area_level_4',
  'administrative_area_level_5',
  'colloquial_area',
  'country',
  'floor',
  'geocode',
  'intersection',
  'locality',
  'neighborhood',
  'point_of_interest',
  'post_box',
  'postal_code',
  'postal_code_prefix',
  'postal_code_suffix',
  'postal_town',
  'premise',
  'room',
  'route',
  'street_address',
  'street_number',
  'sublocality',
  'sublocality_level_1',
  'sublocality_level_2',
  'sublocality_level_3',
  'sublocality_level_4',
  'sublocality_level_5',
  'subpremise',
  'street_address',
  'street_number',
  'route',
  'intersection',
  'political',
  'country',
  'administrative_area_level_1',
  'administrative_area_level_2',
  'administrative_area_level_3',
  'administrative_area_level_4',
  'administrative_area_level_5',
  'colloquial_area',
  'locality',
  'ward',
  'sublocality',
  'sublocality_level_1',
  'sublocality_level_5',
  'neighborhood',
  'premise',
  'subpremise',
  'postal_code',
  'postal_code_suffix',
  'natural_feature',
  'airport',
  'park',
  'floor',
  'establishment',
  'parking',
  'post_box',
  'postal_town',
  'room',
  'street_number',
  'bus_station',
  'train_station',
  'transit_station'
];

export default InlineForm;
