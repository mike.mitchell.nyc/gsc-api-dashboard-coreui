// if address components then get them. If not, don't worry about it.
address_components
  ? address_components.map(component => {
      const long_name = _.get(component, "long_name");
      const short_name = _.get(component, "short_name");
      const types = _.get(component, "types");

      // console.log(long_name, short_name, types);
    })
  : "";

// handles types for Places query
const handleTypes = types.map(type => {
  switch (type) {
    case "street_address":
      addressData.street_address = [long_name, short_name];
      break;
    case "street_number":
      addressData.addressLineOne = [long_name, short_name];
      break;
    case "route":
      addressData.streetName = [long_name, short_name];
      break;
    case "intersection":
      addressData.intersection = [long_name, short_name];
      break;
    case "political":
      addressData.city.political = [long_name, short_name];
      break;
    case "country":
      addressData.country = [long_name, short_name];
      break;
    case "administrative_area_level_1":
      addressData.administrative_area_level_1 = [long_name, short_name];
      break;
    case "administrative_area_level_2":
      addressData.administrative_area_level_2 = [long_name, short_name];
      break;
    case "administrative_area_level_3":
      addressData.administrative_area_level_3 = [long_name, short_name];
      break;
    case "administrative_area_level_4":
      addressData.administrative_area_level_4 = [long_name, short_name];
      break;
    case "administrative_area_level_5":
      addressData.administrative_area_level_5 = [long_name, short_name];
      break;
    case "colloquial_area":
      addressData.colloquial_area = [long_name, short_name];
      break;
    case "locality":
      addressData.city.locality = [long_name, short_name];
      break;
    case "ward":
      addressData.ward = [long_name, short_name];
      break;
    case "sublocality":
      addressData.sublocality = [long_name, short_name];
      break;
    case "sublocality_level_1":
      addressData.sublocality_level_1 = [long_name, short_name];
      break;
    case "sublocality_level_5":
      addressData.sublocality_level_5 = [long_name, short_name];
      break;
    case "neighborhood":
      addressData.neighborhood = [long_name, short_name];
      break;
    case "premise":
      addressData.premise = [long_name, short_name];
      break;
    case "subpremise":
      addressData.subpremise = [long_name, short_name];
      break;
    case "postal_code":
      addressData.zipCode = [long_name, short_name];
      break;
    case "postal_code_suffix":
      addressData.postal_code_suffix = [long_name, short_name];
      break;
    case "natural_feature":
      addressData.natural_feature = [long_name, short_name];
      break;
    case "airport":
      addressData.airport = [long_name, short_name];
      break;
    case "park":
      addressData.park = [long_name, short_name];
      break;

    case "floor":
      address.floor = [long_name, short_name];
      break;
    case "establishment":
      address.establishment = [long_name, short_name];
      break;
    case "point_of_interest":
      address.point_of_interest = [long_name, short_name];
      break;
    case "parking":
      address.parking = [long_name, short_name];
      break;
    case "post_box":
      address.post_box = [long_name, short_name];
      break;
    case "postal_town":
      address.postal_town = [long_name, short_name];
      break;
    case "room":
      address.room = [long_name, short_name];
      break;
    case "street_number":
      address.street_number = [long_name, short_name];
      break;
    case "bus_station":
      address.bus_station = [long_name, short_name];
      break;
    case "train_station":
      address.train_station = [long_name, short_name];
      break;
    case "transit_station":
      address.transit_station = [long_name, short_name];
      break;
    default:
      console.log("you done fucked up and missed something ", type);
      break;
  }
});

const untrueCategories = [
  "administrative_area_level_1",
  "administrative_area_level_2",
  "administrative_area_level_3",
  "administrative_area_level_4",
  "administrative_area_level_5",
  "colloquial_area",
  "country",
  "floor",
  "geocode",
  "intersection",
  "locality",
  "neighborhood",
  "point_of_interest",
  "post_box",
  "postal_code",
  "postal_code_prefix",
  "postal_code_suffix",
  "postal_town",
  "premise",
  "room",
  "route",
  "street_address",
  "street_number",
  "sublocality",
  "sublocality_level_1",
  "sublocality_level_2",
  "sublocality_level_3",
  "sublocality_level_4",
  "sublocality_level_5",
  "subpremise",
  "street_address",
  "street_number",
  "route",
  "intersection",
  "political",
  "country",
  "administrative_area_level_1",
  "administrative_area_level_2",
  "administrative_area_level_3",
  "administrative_area_level_4",
  "administrative_area_level_5",
  "colloquial_area",
  "locality",
  "ward",
  "sublocality",
  "sublocality_level_1",
  "sublocality_level_5",
  "neighborhood",
  "premise",
  "subpremise",
  "postal_code",
  "postal_code_suffix",
  "natural_feature",
  "airport",
  "park",
  "floor",
  "establishment",
  "point_of_interest",
  "parking",
  "post_box",
  "postal_town",
  "room",
  "street_number",
  "bus_station",
  "train_station",
  "transit_station"
];
