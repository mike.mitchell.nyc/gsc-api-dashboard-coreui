import React from "react";
import {
  Input,
  InputGroup,
  InputGroupAddon,
  FormGroup,
  Label,
  Col,
  FormText
} from "reactstrap";

export const NormalFormField = ({
  type,
  id,
  cssClass,
  placeholder,
  name,
  value,
  onChangeValue,
  required,
  pattern,
  title,
  icon,
  label,
  helpText
}) => {
  return (
    <Col md={6}>
      <Label for={id} className="text-left">
        {label.text}
      </Label>
      <Input
        type={type}
        id={id}
        className={`form-control w-100 ${cssClass}`}
        placeholder={placeholder}
        name={id}
        value={value}
        onChange={event => onChangeValue(event)}
        required={required}
        pattern={pattern || ".*"}
        title={title || ""}
      />
      <FormText className="help-block">{helpText || ""}</FormText>
    </Col>
  );
};
