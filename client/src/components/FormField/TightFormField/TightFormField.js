import React from "react";
import {
  Input,
  InputGroup,
  InputGroupAddon,
  FormGroup,
  Label,
  Col
} from "reactstrap";

export const TightFormField = ({
  type,
  id,
  cssClass,
  placeholder,
  name,
  value,
  onChangeValue,
  required,
  pattern,
  title,
  icon,
  label
}) => {
  return (
    <Col md={6}>
      <FormGroup className="mb-2 mr-sm-2 mt-2">
        <Col md={4}>
          <Label for={id} className="text-left">
            {label.text}
          </Label>
        </Col>
        <Col md={8}>
          <Input
            type={type}
            id={id}
            className={`form-control w-100 ${cssClass}`}
            placeholder={placeholder}
            name={id}
            value={value}
            onChange={event => onChangeValue(event)}
            required={required}
            pattern={pattern || ".*"}
            title={title || ""}
          />
        </Col>
      </FormGroup>
    </Col>
  );
};
