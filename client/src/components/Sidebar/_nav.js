export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer" // ,
      //   badge: {
      //     variant: "info",
      //     text: "NEW"
      //   }
    },
    {
      title: true,
      name: "Services",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "" // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "GSC Advanced",
      url: "/gsc-advanced",
      icon: "icon-pie-chart"
    },
    {
      divider: true
    },
    {
      name: "Citation Manager",
      url: "/citation-manager",
      icon: "icon-star"
    },
    {
      divider: true
    },
    {
      name: "Settings",
      url: "/settings",
      icon: "icon-settings"
      //   class: "mt-auto",
      //   variant: "success"
    }
  ]
};
