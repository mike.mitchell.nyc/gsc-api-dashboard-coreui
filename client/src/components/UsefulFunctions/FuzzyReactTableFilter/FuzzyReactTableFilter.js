import fuzzysort from "fuzzysort";
import * as lodash from "lodash";

export function customFilterUsingFuzzySearch(filter, rows) {
  let newRows = [];
  if (rows.length > 0) {
    if (Array.isArray(rows[0][filter.id])) {
      console.log(rows[0][filter.id]);
      rows.forEach(row =>
        row[filter.id].forEach(arrayValue => {
          row[filter.id] = arrayValue;
          // console.log(row);
        })
      );
    }
    newRows = fuzzysort
      .go(filter.value, rows, {
        key: filter.id
      })
      .map(result => result.obj);
  }
  return newRows;
}
