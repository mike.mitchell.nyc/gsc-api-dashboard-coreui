

import moment from "moment";
import _ from "lodash";
import { decimalAdjust } from "../DecimalAdjust/DecimalAdjust";

const round10 = function(value: number, exp: number): string {
  return decimalAdjust("round", value, exp);
};

export function gscDataGenerator(pouchData: Array<Object>) {
  console.log(pouchData);
  const flattenedResults = pouchData
    .map(row => row.doc.modifiedRows)
    .reduce(function(accumulator, currentValue) {
      return accumulator.concat(currentValue);
    }, []);

  console.log(flattenedResults, "flattened results");

  const summedClicks = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) => {
      return accumulator + currentValue.clicks;
    },
    0
  );

  const summedImpressions = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) => {
      return accumulator + currentValue.impressions;
    },
    0
  );

  const summedPositions = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) => {
      return accumulator + currentValue.position;
    },
    0
  );

  const averagePosition = round10(
    summedPositions / flattenedResults.length * 10,
    -2
  );

  const summedCTR = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) => {
      return accumulator + currentValue.ctr;
    },
    0
  );

  const averageCTR = round10(summedCTR / flattenedResults.length * 10, -2);

  const topThreePosition = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) =>
      currentValue.position <= 3 ? (accumulator += 1) : accumulator,
    0
  );

  const threeToTenPosition = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) =>
      currentValue.position >= 4 && currentValue.position <= 10
        ? (accumulator += 1)
        : accumulator,
    0
  );

  const elevenToFiftyPosition = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) =>
      currentValue.position >= 10 && currentValue.position <= 50
        ? (accumulator += 1)
        : accumulator,
    0
  );

  const oneToFiftyPosition = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) =>
      currentValue.position <= 50 ? (accumulator += 1) : accumulator,
    0
  );

  const uniquePages = [...new Set(flattenedResults.map(item => item.page))];
  const siteList = [...new Set(flattenedResults.map(item => item.site))];

  const siteCount = siteList.length;
  const pageCount = uniquePages.length;

  const totalKeywords = Object.keys(
    _(flattenedResults)
      .groupBy("keyword")
      .value()
  ).length;

  console.log(totalKeywords);

  // search the flattenedResults by date and create a new object based on the dates
  const groupedByDate = _(flattenedResults)
    .groupBy("date")
    .map(dateDataCombined => dateDataCombined)
    .value();

  // combine all data for dates as an array [date, clicks, impressions, ctr, position]
  const datesCombined = groupedByDate.map(dataCombined => {
    const clicksData = dataCombined.reduce(
      (accumulator, currentValue, currentIndex, array) => {
        return accumulator + currentValue.clicks;
      },
      0
    );

    const impressionsData = dataCombined.reduce(
      (accumulator, currentValue, currentIndex, array) => {
        return accumulator + currentValue.impressions;
      },
      0
    );

    const ctrData = dataCombined.reduce(
      (accumulator, currentValue, currentIndex, array) => {
        return accumulator + currentValue.ctr;
      },
      0
    );

    const positionData = dataCombined.reduce(
      (accumulator, currentValue, currentIndex, array) => {
        return accumulator + currentValue.position;
      },
      0
    );

    return [
      dataCombined[0].date,
      clicksData,
      impressionsData,
      round10(ctrData / dataCombined.length * 100, -2),
      round10(positionData / dataCombined.length, -2)
    ];
  });

  // create x,y values for chartjs for clicks, impressions, ctr, position

  const labels = datesCombined.map(data =>
    moment(data[0]).format("MMM DD, YYYY")
  );

  const clicksCombined = datesCombined.map(data => {
    return {
      t: moment(data[0]),
      y: data[1]
    };
  });

  const impressionsCombined = datesCombined.map(data => {
    return {
      t: moment(data[0]),
      y: data[2]
    };
  });

  const ctrCombined = datesCombined.map(data => {
    return {
      t: moment(data[0]),
      y: data[3]
    };
  });

  const positionsCombined = datesCombined.map(data => {
    return {
      t: moment(data[0]),
      y: data[4]
    };
  });

  return {
    clicks: clicksCombined,
    impressions: impressionsCombined,
    ctr: ctrCombined,
    position: positionsCombined,
    originalData: flattenedResults,
    xAxesLabels: labels,
    isLoaded: true,
    summedClicks,
    averageCTR,
    summedImpressions,
    averagePosition,
    topThreePosition,
    threeToTenPosition,
    elevenToFiftyPosition,
    pageCount,
    totalKeywords,
    siteList,
    siteCount,
    oneToFiftyPosition
  };
}
