import React, { Component } from "react";

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

import { customFilterUsingFuzzySearch } from "../../components/UsefulFunctions/FuzzyReactTableFilter/FuzzyReactTableFilter";

const citationList = require("./citations.json");

const CitationList = props => {
  return (
    <div className="animated fadeIn">
      {console.log("citation manager citationlist", citationList)}
      <ReactTable
        data={citationList}
        filterable
        defaultFilterMethod={(filter, row) =>
          String(row[filter.id].join(" ")) === filter.value
        }
        columns={[
          {
            Header: "Citation Manager",
            columns: [
              {
                id: "directory",
                Header: "Directory or Search Engine",
                filterMethod: (filter, rows) =>
                  customFilterUsingFuzzySearch(filter, rows),
                accessor: "domainAndPath",
                filterAll: true
              },
              {
                id: "categories",
                filterMethod: (filter, rows) =>
                  customFilterUsingFuzzySearch(filter, rows),
                Header: "Categories",
                accessor: doc =>
                  doc.categories
                    .map(
                      category =>
                        category[0]
                          ? category[0].toUpperCase() + category.slice(1)
                          : ""
                    )
                    .join(", "),
                filterAll: true
              },
              {
                id: "regions",
                Header: "Regions",
                filterMethod: (filter, rows) =>
                  customFilterUsingFuzzySearch(filter, rows),
                accessor: doc =>
                  doc.Location.map(
                    location => (location[0] ? location.toUpperCase() : "")
                  ).join(", "),
                filterAll: true
              },
              {
                id: "cost",
                Header: "Cost",
                accessor: doc => doc.cost.toLowerCase(),
                filterMethod: (filter, rows) =>
                  customFilterUsingFuzzySearch(filter, rows),
                filterAll: true,
                width: 150
              }
            ]
          }
        ]}
        defaultPageSize={20}
        className="-striped -highlight -responsive ReactTable"
      />
    </div>
  );
};

export default CitationList;
