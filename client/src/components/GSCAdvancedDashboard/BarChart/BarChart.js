import * as React from "react";
import { Row, Col, Card, CardBody } from "reactstrap";
import { Bar } from "react-chartjs-2";
const BarChart = props => {
  return (
    <Row className="justify-content-center">
      <Col md="10">
        <Card>
          <CardBody>
            <div className="chart-wrapper">
              <Bar
                height={75}
                data={{
                  type: "bar",
                  labels: props.data.xAxesLabels,
                  datasets: [
                    {
                      label: "Clicks",
                      type: "line",
                      fill: false,
                      lineTension: 0.1,
                      backgroundColor: "rgba(77,144,254,0.4)",
                      borderColor: "rgba(77,144,254,1)",
                      borderCapStyle: "butt",
                      borderDash: [],
                      borderDashOffset: 0.0,
                      borderJoinStyle: "miter",
                      pointBorderColor: "rgba(77,144,254,1)",
                      pointBackgroundColor: "#fff",
                      pointBorderWidth: 1,
                      pointHoverRadius: 5,
                      pointHoverBackgroundColor: "rgba(77,144,254,1)",
                      pointHoverBorderColor: "rgba(220,220,220,1)",
                      pointHoverBorderWidth: 2,
                      pointRadius: 1,
                      pointHitRadius: 10,
                      data: props.data.clicks,
                      yAxisID: "y-axis-1"
                    },
                    {
                      label: "Impressions",
                      fill: false,
                      type: "line",
                      lineTension: 0.1,
                      backgroundColor: "rgba(221,75,57,0.4)",
                      borderColor: "rgba(221,75,57,1)",
                      borderCapStyle: "butt",
                      borderDash: [],
                      borderDashOffset: 0.0,
                      borderJoinStyle: "miter",
                      pointBorderColor: "rgba(221,75,57,1)",
                      pointBackgroundColor: "#fff",
                      pointBorderWidth: 1,
                      pointHoverRadius: 5,
                      pointHoverBackgroundColor: "rgba(221,75,57,1)",
                      pointHoverBorderColor: "rgba(220,220,220,1)",
                      pointHoverBorderWidth: 2,
                      pointRadius: 1,
                      pointHitRadius: 10,
                      data: props.data.impressions,
                      yAxisID: "y-axis-1"
                    },
                    {
                      label: "CTR",
                      type: "line",
                      fill: false,
                      lineTension: 0.1,
                      backgroundColor: "rgba(255,153,0,0.4)",
                      borderColor: "rgba(255,153,0,1)",
                      borderCapStyle: "butt",
                      borderDash: [],
                      borderDashOffset: 0.0,
                      borderJoinStyle: "miter",
                      pointBorderColor: "rgba(255,153,0,1)",
                      pointBackgroundColor: "#fff",
                      pointBorderWidth: 1,
                      pointHoverRadius: 5,
                      pointHoverBackgroundColor: "rgba(255,153,0,1)",
                      pointHoverBorderColor: "rgba(220,220,220,1)",
                      pointHoverBorderWidth: 2,
                      pointRadius: 1,
                      pointHitRadius: 10,
                      data: props.data.ctr,
                      yAxisID: "y-axis-2"
                    },
                    {
                      label: "Position",
                      type: "line",
                      fill: false,
                      lineTension: 0.1,
                      backgroundColor: "rgba(16,150,24,0.4)",
                      borderColor: "rgba(16,150,24,1)",
                      borderCapStyle: "butt",
                      borderDash: [],
                      borderDashOffset: 0.0,
                      borderJoinStyle: "miter",
                      pointBorderColor: "rgba(16,150,24,1)",
                      pointBackgroundColor: "#fff",
                      pointBorderWidth: 1,
                      pointHoverRadius: 5,
                      pointHoverBackgroundColor: "rgba(16,150,24,1)",
                      pointHoverBorderColor: "rgba(220,220,220,1)",
                      pointHoverBorderWidth: 2,
                      pointRadius: 1,
                      pointHitRadius: 10,
                      data: props.data.position,
                      yAxisID: "y-axis-1"
                    }
                  ]
                }}
                options={{
                  maintainAspectRatio: false,
                  tooltips: {
                    mode: "index",
                    intersect: true
                  },
                  scales: {
                    xAxes: [
                      {
                        display: true,
                        // scaleLabel: {
                        //   display: true,
                        //   labelString: "Date"
                        // },
                        time: {
                          unit: "month"
                        }
                      }
                    ],
                    yAxes: [
                      {
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "left",
                        scaleLabel: {
                          display: true,
                          labelString: "Clicks, Impressions, Position"
                        },
                        id: "y-axis-1"
                      },
                      {
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "right",
                        id: "y-axis-2",
                        scaleLabel: {
                          display: true,
                          labelString: "CTR"
                        },
                        gridLines: {
                          drawOnChartArea: false
                        }
                      }
                    ]
                  }
                }}
              />
            </div>
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
};

export default BarChart;
