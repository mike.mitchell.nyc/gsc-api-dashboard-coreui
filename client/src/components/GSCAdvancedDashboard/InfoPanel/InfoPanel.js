// @flow

import * as React from "react";
import InfoCard from "./InfoCard/InfoCard";
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  CardColumns,
  CardGroup,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

type Props = {
  match: Object,
  location: Object,
  history: Object,
  staticContext: Object,
  data: {
    averageCTR: number,
    averagePosition: number,
    clicks: [],
    ctr: [],
    data: [],
    elevenToFiftyPosition: number,
    impressions: [],
    isLoaded: boolean,
    oneToFiftyPosition: number,
    originalData: [],
    pageCount: number,
    position: [],
    siteCount: number,
    siteList: string[],
    sitesToFilter: [],
    summedClicks: number,
    summedImpressions: number,
    targets: [],
    threeToTenPosition: number,
    topThreePosition: number,
    totalKeywords: number,
    xAxesLabels: []
  }
};

const InfoPanel = (props: Props) => {
  return (
    <Row className="justify-content-center">
      <Col xs="10">
        <CardGroup className="mb-1">
          <InfoCard
            icon="icon-people"
            color="primary"
            value="25"
            header={props.data.totalKeywords}
          >
            Keyword Count
          </InfoCard>
          <InfoCard
            icon="icon-graph"
            color="danger"
            header="385"
            value="25"
            header={props.data.summedClicks}
          >
            Clicks
          </InfoCard>
          <InfoCard
            icon="icon-chemistry"
            color="warning"
            header="1238"
            header={props.data.averageCTR}
            value="25"
          >
            Avg. Click Through Rate
          </InfoCard>
          <InfoCard
            icon="icon-user-follow"
            color="success"
            header="28%"
            value="25"
            header={props.data.summedImpressions}
          >
            Impressions
          </InfoCard>
          <InfoCard
            icon="fa fa-book"
            color="danger"
            value="25"
            header={props.data.pageCount}
          >
            Unique Pages Ranking
          </InfoCard>
        </CardGroup>
        <CardGroup className="mb-4">
          <InfoCard
            icon="icon-chart"
            color="info"
            value="25"
            header={props.data.averagePosition}
          >
            Avg. Position
          </InfoCard>
          <InfoCard
            icon="fa fa-battery-4"
            color="success"
            value="25"
            header={props.data.topThreePosition}
          >
            Keywords Ranked: 1-3
          </InfoCard>
          <InfoCard
            icon="fa fa-battery-3"
            color="warning"
            value="25"
            header={props.data.threeToTenPosition}
          >
            Keywords Ranked: 4-10
          </InfoCard>
          <InfoCard
            icon="fa fa-battery-1"
            color="primary"
            value="25"
            header={props.data.elevenToFiftyPosition}
          >
            Keywords Ranked: 11-50
          </InfoCard>
          <InfoCard
            icon="icon-speedometer"
            color="danger"
            header={props.data.siteCount}
            value="25"
          >
            Sites Ranked
          </InfoCard>
        </CardGroup>
      </Col>
    </Row>
  );
};

export default InfoPanel;
