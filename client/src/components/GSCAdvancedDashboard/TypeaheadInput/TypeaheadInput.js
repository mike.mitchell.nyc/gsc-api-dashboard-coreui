import * as React from "react";
import { Row, Col } from "reactstrap";
import { Typeahead, Token } from "react-bootstrap-typeahead";

const TypeaheadInput = props => {
  return (
    <Row className="justify-content-center mb-4">
      <Col xs="10">
        <Typeahead
          multiple
          options={props.data}
          placeholder="Filter sites..."
          clearButton
          selectHintOnEnter
          onChange={option => props.filterData(option)}
          renderToken={(option, onRemove, index) => {
            return (
              <Token
                key={index}
                className="rbt-token rbt-token-removeable"
                onRemove={onRemove.onRemove}
              >
                {option}
              </Token>
            );
          }}
        />
      </Col>
    </Row>
  );
};

export default TypeaheadInput;
