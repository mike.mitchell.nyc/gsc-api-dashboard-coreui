import * as React from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { customFilterUsingFuzzySearch } from "../../UsefulFunctions/FuzzyReactTableFilter/FuzzyReactTableFilter";
import { decimalAdjust } from "../../UsefulFunctions/DecimalAdjust/DecimalAdjust";
import { Tooltip } from "reactstrap";

// Decimal round
const round10 = function(value, exp) {
  return decimalAdjust("round", value, exp);
};

const GSCAdvancedTable = props => {
  return (
    <ReactTable
      data={props.data}
      filterable
      columns={[
        {
          Header: "Citation Data",
          columns: [
            {
              id: "keyword",
              Header: "Keyword",
              accessor: doc => doc.keyword,
              filterMethod: (filter, rows) =>
                customFilterUsingFuzzySearch(filter, rows),
              filterAll: true
            },
            {
              id: "site",
              Header: "Site",
              accessor: doc => doc.site,
              width: 350,
              filterMethod: (filter, rows) => {
                return customFilterUsingFuzzySearch(filter, rows);
              },
              filterAll: true
            },
            {
              id: "path",
              Header: "Path",
              accessor: doc => doc.path,
              filterMethod: (filter, rows) =>
                customFilterUsingFuzzySearch(filter, rows),
              filterAll: true,
              Cell: row => (
                <div
                  style={{
                    width: "100%",
                    height: "100%"
                    // backgroundColor: "#dadada",
                    // borderRadius: "2px"
                  }}
                  id={row.original.id}
                >
                  {row.value}
                  {/*<div
                    style={{
                      width: `${row.value}%`,
                      height: "100%",
                      backgroundColor:
                        row.value > 66
                          ? "#85cc00"
                          : row.value > 33
                            ? "#ffbf00"
                            : "#ff2e00",
                      borderRadius: "2px",
                      transition: "all .2s ease-out"
                    }}
                  />*/}
                  <Tooltip
                    placement="right"
                    isOpen={props.tooltipOpen}
                    target={row.original.id}
                    toggle={props.tooltipToggle}
                  >
                    {`Total Pages: ${row.original.uniquePaths.length}`}
                    {row.original.uniquePaths.map(path => path)}
                    {console.log(row.original)}
                  </Tooltip>
                </div>
              )
            },
            {
              id: "clicks",
              Header: "Clicks",
              accessor: doc => doc.clicks,
              width: 100,
              filterMethod: (filter, rows) =>
                customFilterUsingFuzzySearch(filter, rows),
              filterAll: true
            },
            {
              id: "ctr",
              Header: "Click Through Rate (%)",
              accessor: doc => round10(doc.ctr * 100, -2),
              width: 100,
              filterMethod: (filter, rows) =>
                customFilterUsingFuzzySearch(filter, rows),
              filterAll: true
            },
            {
              id: "impressions",
              Header: "Impressions",
              accessor: doc => doc.impressions,
              width: 100,
              filterMethod: (filter, rows) =>
                customFilterUsingFuzzySearch(filter, rows),
              filterAll: true
            },
            {
              id: "position",
              Header: "Position",
              accessor: doc => round10(doc.position, -2),
              width: 100,
              filterMethod: (filter, rows) =>
                customFilterUsingFuzzySearch(filter, rows),
              filterAll: true
            },
            {
              id: "date",
              Header: "Date",
              accessor: doc => doc.date,
              width: 100,
              filterMethod: (filter, rows) =>
                customFilterUsingFuzzySearch(filter, rows),
              filterAll: true
            }
          ]
        }
      ]}
      getTrProps={(state, rowInfo, column, instance) => {
        return {
          // onMouseEnter: (e, handleOriginal) => {
          //   console.log("it produced this event:", e);
          //   console.log("It was in this column:", column);
          //   console.log(
          //     "It was in this row:",
          //     rowInfo /*.original[column.id]*/
          //   );
          //   console.log("It was in this table instance:", instance);
          //   switch (column.id) {
          //     case "path":
          //       console.log(
          //         "the path you are looking for is rowInfo.original[column.id]",
          //         rowInfo.original.uniquePaths
          //       );
          //       break;
          //     default:
          //       console.log("column.id");
          //   }

          //   // IMPORTANT! React-Table uses onClick internally to trigger
          //   // events like expanding SubComponents and pivots.
          //   // By default a custom 'onClick' handler will override this functionality.
          //   // If you want to fire the original onClick handler, call the
          //   // 'handleOriginal' function.
          //   if (handleOriginal) {
          //     handleOriginal();
          //   }
          // },
          onClick: (e, handleOriginal) => {
            console.log("it produced this event:", e);
            console.log("It was in this column:", column);
            console.log(
              "It was in this row:",
              rowInfo.original.id /*.original[column.id]*/
            );
            // return (
            //   <UncontrolledTooltip
            //   placement="right"
            //   target={rowInfo.original.id}
            // >
            //   Hello world!
            // </UncontrolledTooltip>
            // );
            // switch (column.id) {
            //   case "path":
            //     console.log(
            //       "the path you are looking for is rowInfo.original[column.id]",
            //       rowInfo.original.uniquePaths
            //     );
            //     break;
            //   default:
            //     console.log("column.id");
            // }

            // IMPORTANT! React-Table uses onClick internally to trigger
            // events like expanding SubComponents and pivots.
            // By default a custom 'onClick' handler will override this functionality.
            // If you want to fire the original onClick handler, call the
            // 'handleOriginal' function.
            if (handleOriginal) {
              handleOriginal();
            }
          }
        };
      }}
      className="-striped -highlight"
    />
  );
};

export default GSCAdvancedTable;
