import React from "react";
import { Input, InputGroup, InputGroupAddon, FormText } from "reactstrap";

export const BusinessName = props => {
  return (
    <div>
      <InputGroup>
        <InputGroupAddon>
          <span className="input-group-text">
            <i className="icon-user" />
          </span>
        </InputGroupAddon>
        <Input
          type="text"
          id="name"
          className="form-control"
          placeholder="Business Name"
          name="name"
          value={props.value}
          onChange={event => props.onChangeValue(event)}
          required
        />
      </InputGroup>
    </div>
  );
};

export const BusinessAddressLineOne = props => {
  return (
    <div>
      <InputGroup>
        <InputGroupAddon>
          <span className="input-group-text">
            <i className="icon-user" />
          </span>
        </InputGroupAddon>
        <Input
          type="text"
          id="addressLineOne"
          className="form-control"
          placeholder="Address Line 1: 123 Mulberry Street"
          name="addressLineOne"
          value={props.value}
          onChange={event => props.onChangeValue(event)}
          required
        />
      </InputGroup>
    </div>
  );
};

export const BusinessAddressLineTwo = props => {
  return (
    <div>
      <InputGroup>
        <InputGroupAddon>
          <span className="input-group-text">
            <i className="icon-user" />
          </span>
        </InputGroupAddon>
        <Input
          type="text"
          id="addressLineTwo"
          className="form-control"
          placeholder="Address Line 2: Apartment, suite , unit, building, floor, etc."
          name="addressLineTwo"
          value={props.value}
          onChange={event => props.onChangeValue(event)}
          required
        />
      </InputGroup>
    </div>
  );
};

export const City = props => {
  return (
    <div>
      <InputGroup>
        <InputGroupAddon>
          <span className="input-group-text">
            <i className="icon-user" />
          </span>
        </InputGroupAddon>
        <Input
          type="text"
          id="city"
          className="form-control"
          placeholder="City"
          name="city"
          value={props.value}
          onChange={event => props.onChangeValue(event)}
          required
        />
      </InputGroup>
    </div>
  );
};

export const State = props => {
  return (
    <div>
      <InputGroup>
        <InputGroupAddon>
          <span className="input-group-text">
            <i className="icon-user" />
          </span>
        </InputGroupAddon>
        <Input
          type="text"
          id="state"
          className="form-control"
          placeholder="State"
          name="state"
          value={props.value}
          onChange={event => props.onChangeValue(event)}
          required
        />
      </InputGroup>
    </div>
  );
};

export const ZipCode = props => {
  return (
    <div>
      <InputGroup>
        <InputGroupAddon>
          <span className="input-group-text">
            <i className="icon-user" />
          </span>
        </InputGroupAddon>
        <Input
          type="text"
          id="zipcode"
          className="form-control"
          placeholder="Zip Code"
          name="zipcode"
          value={props.value}
          onChange={event => props.onChangeValue(event)}
          required
        />
      </InputGroup>
    </div>
  );
};

export const Phone = props => {
  return (
    <div>
      <InputGroup>
        <InputGroupAddon>
          <span className="input-group-text">
            <i className="icon-user" />
          </span>
        </InputGroupAddon>
        <Input
          type="text"
          id="phone"
          className="form-control"
          placeholder="PhoneNumber"
          name="phone"
          value={props.value}
          onChange={event => props.onChangeValue(event)}
          required
        />
      </InputGroup>
    </div>
  );
};

export const Homepage = props => {
  return (
    <div>
      <InputGroup>
        <InputGroupAddon>
          <span className="input-group-text">
            <i className="icon-user" />
          </span>
        </InputGroupAddon>
        <Input
          type="text"
          id="homepage"
          className="form-control"
          placeholder="URL of Homepage: https://mypage.net/"
          name="homepage"
          value={props.value}
          onChange={event => props.onChangeValue(event)}
          required
        />
      </InputGroup>
    </div>
  );
};
