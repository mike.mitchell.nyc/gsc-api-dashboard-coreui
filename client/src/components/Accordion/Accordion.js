import React, { Component } from "react";
import { Collapse, Button, CardBody, Card, Col } from "reactstrap";

class Accordion extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { collapse: false };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  render() {
    return (
      <div>
        <div className="d-flex flex-row-reverse">
          <Button
            color="primary"
            onClick={this.toggle}
            style={{ marginBottom: "1rem" }}
          >
            Toggle
          </Button>
        </div>
        <div className="d-flex flex-row-reverse">
          <Col md="6">
            <Collapse isOpen={this.state.collapse}>
              <Card>
                <CardBody>{this.props.passedComponent}</CardBody>
              </Card>
            </Collapse>
          </Col>
        </div>
      </div>
    );
  }
}

export default Accordion;
