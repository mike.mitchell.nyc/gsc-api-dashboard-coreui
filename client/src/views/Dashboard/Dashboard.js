// @flow

import React, { Component } from "react";
import { Bar, Line } from "react-chartjs-2";
import {
    Badge,
    Row,
    Col,
    Progress,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardTitle,
    Button,
    ButtonToolbar,
    ButtonGroup,
    ButtonDropdown,
    Label,
    Input,
    Table
} from "reactstrap";
import PouchDB from "pouchdb";

import { Props } from "../../SharedTypes";

class Dashboard extends Component<Props> {
    constructor(props: Props) {
        super(props);
    }
    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col xs="12" sm="6" lg="3">
                        <Card className="text-white bg-primary">
                            Horses of courses
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Dashboard;
