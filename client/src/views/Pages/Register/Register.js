


import React, { Component, SyntheticEvent } from "react";
import { Link } from "react-router-dom";
import superlogin from "superlogin-client";
import { SuperLoginClientConfig } from "../../../sl-client-config";
import {
    Form,
    FormGroup,
    Container,
    Row,
    Col,
    Card,
    CardBody,
    CardFooter,
    Button
} from "reactstrap";
import {
    UsernameField,
    FirstNameField,
    LastNameField,
    CompanyField,
    EmailField,
    PasswordField,
    VerifiedPasswordField,
    emailRegex,
    usernameRegex
} from "../../../components/Inputs/Auth";

superlogin.configure(SuperLoginClientConfig);

type Props = {
    match: Object,
    location: Object,
    history: Object,
    staticContext: Object
};

type State = {
    username: string,
    firstName: string,
    lastName: string,
    company: string,
    email: string,
    password: string,
    confirmPassword: string
};

class Register extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            username: "",
            firstName: "",
            lastName: "",
            company: "",
            email: "",
            password: "",
            confirmPassword: ""
        };
        
        this.handleUserInput = this.handleUserInput.bind(this);
        
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleUserInput(e) {
        (e.currentTarget);
        e.persist();
        const name = e.target.name;

        const value = e.target.value;

        // HTML5 Form Password Match Validation

        if (name === "confirmPassword") {
            if (value != this.state.password) {
                e.target.setCustomValidity("Passwords do not match");
            } else {
                e.target.setCustomValidity("");
            }
        }

        // Username already exists

        if (
            name === "username" &&
            value.length >= 6 &&
            value.length <= 15 &&
            value.match(usernameRegex)
        ) {
            superlogin
                .validateUsername(value)
                .then(function() {
                    e.target.setCustomValidity("");
                })
                .catch(function(err) {
                    if (err) {
                        return e.target.setCustomValidity(
                            "Username already in use"
                        );
                    }
                });
        }

        // Email already exists

        if (name === "email" && value.match(emailRegex)) {
            superlogin
                .validateEmail(value)
                .then(e.target.setCustomValidity(""))
                .catch(function(err) {
                    if (err) {
                        return e.target.setCustomValidity(
                            "Email already in use"
                        );
                    }
                });
        }

        // set state

        this.setState({ [name]: value });
    }

    handleFormSubmit(e: SyntheticEvent<HTMLButtonElement>) {
        e.preventDefault();
        superlogin
            .register(this.state)
            .then(response => this.props.history.push("/login"))
            .catch(err => console.log(err.message));
    }

    render() {
        return (
            <div className="app flex-row align-items-center">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="6">
                            <Card className="mx-4">
                                <Form onSubmit={this.handleFormSubmit}>
                                    <CardBody className="p-4">
                                        <h1 className="text-center">
                                            Create your account
                                        </h1>
                                        <p className="text-center text-muted">
                                            Fill out the following information
                                        </p>
                                        <FormGroup>
                                            <UsernameField
                                                value={this.state.username}
                                                onChangeValue={
                                                    this.handleUserInput
                                                }
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <EmailField
                                                value={this.state.email}
                                                onChangeValue={
                                                    this.handleUserInput
                                                }
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <PasswordField
                                                value={this.state.password}
                                                onChangeValue={
                                                    this.handleUserInput
                                                }
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <VerifiedPasswordField
                                                value={
                                                    this.state.confirmPassword
                                                }
                                                onChangeValue={
                                                    this.handleUserInput
                                                }
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <FirstNameField
                                                value={this.state.firstName}
                                                onChangeValue={
                                                    this.handleUserInput
                                                }
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <LastNameField
                                                value={this.state.lastName}
                                                onChangeValue={
                                                    this.handleUserInput
                                                }
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <CompanyField
                                                value={this.state.company}
                                                onChangeValue={
                                                    this.handleUserInput
                                                }
                                            />
                                        </FormGroup>
                                        <Row className="mb-2">
                                            <Col className="d-flex justify-content-center">
                                                <Button
                                                    color="success"
                                                    type="submit"
                                                    value="Submit"
                                                    id="submitRegistration"
                                                >
                                                    Create Account
                                                </Button>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col className="d-flex justify-content-center">
                                                <Link to="/forgot-password">
                                                    <Button color="link">
                                                        <span>
                                                            Forgot Password?
                                                        </span>
                                                    </Button>
                                                </Link>
                                            </Col>
                                        </Row>
                                    </CardBody>
                                    <CardFooter className="p-4">
                                        <Row>
                                            <Col className="d-flex justify-content-center">
                                                <Link to="/login">
                                                    <Button color="info">
                                                        <span>
                                                            Already Signed Up?
                                                            Login!
                                                        </span>
                                                    </Button>
                                                </Link>
                                            </Col>
                                        </Row>
                                    </CardFooter>
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Register;
