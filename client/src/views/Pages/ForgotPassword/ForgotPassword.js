import React, { Component } from "react";
import { Link } from "react-router-dom";
import superlogin from "superlogin-client";
import { SuperLoginClientConfig } from "../../../sl-client-config";
import {
  Form,
  FormGroup,
  Container,
  Row,
  Col,
  Card,
  CardGroup,
  CardBody,
  CardFooter,
  Button
} from "reactstrap";
import { EmailField } from "../../../components/Inputs/Auth";
import { debounce } from "lodash";

superlogin.configure(SuperLoginClientConfig);

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      username: ""
    };

    this.handleUserInput = this.handleUserInput.bind(this);

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleUserInput(e) {
    const name = e.target.name;

    const value = e.target.value;

    this.setState({ [name]: value });
  }

  handleFormSubmit(e) {
    e.persist();
    e.preventDefault();

    superlogin
      .forgotPassword(this.state.email)
      .then(function(response) {
        console.log("email sent", response);
      })
      .catch(function(err) {
        if (err) {
          
          console.log("wheres the email", err);
        }
      });
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup>
                <Card className="p-4">
                  <Form onSubmit={this.handleFormSubmit}>
                    <CardBody className="p-4">
                      <h1>Forgot Account Password</h1>
                      <p className="text-muted">
                        Receive an email to change your password
                      </p>
                      <FormGroup>
                        <EmailField
                          value={this.state.username}
                          onChangeValue={this.handleUserInput}
                        />
                      </FormGroup>

                      <Row>
                        <Col md={{ size: 4, offset: 4 }} xs="12">
                          <Button
                            color="success"
                            type="submit"
                            value="Submit"
                            block
                          >
                            Send Email
                          </Button>
                        </Col>
                      </Row>
                    </CardBody>
                    <CardFooter>
                      <Row>
                        <Col className="d-flex mr-auto">
                          <Link to="/login">
                            <Button color="link">
                              <span>Remembered it? Log in here!</span>
                            </Button>
                          </Link>
                        </Col>
                        <Col className="d-flex  mr-auto">
                          <Link to="/register">
                            <Button color="link">
                              <span>Not signed up? Register now!</span>
                            </Button>
                          </Link>
                        </Col>
                      </Row>
                    </CardFooter>
                  </Form>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default ForgotPassword;
