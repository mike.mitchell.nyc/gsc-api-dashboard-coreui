import * as React from "react";
import * as qs from "query-string";
import { Button } from "reactstrap";

const CloseWindow = () => {
  const parsed = qs.parse(location.search);
  let text = "";
  const googleSuccess = () => {
    text += "Registration successful - you can now close this window!";
  };
  const googleDuplicate = () => {
    text += "This account is already registered with us!";
  };
  const windowClose = red => {
    text += red;
  };
  switch (parsed.status) {
    case "GoogleSuccess":
      googleSuccess();
      break;
    case "GoogleDuplicate":
      googleDuplicate();
      break;
    default:
      windowClose(parsed.status);
  }
  return (
    <div
      style={{
        position: "absolute",
        top: "50%",
        left: "50%",
        width: "30em",
        height: "18em"
      }}
    >
      {text}
      <Button onClick={window.close()}>Close Window</Button>
    </div>
  );
};

export default CloseWindow;
