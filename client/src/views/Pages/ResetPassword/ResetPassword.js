


import React, { Component, SyntheticEvent } from "react";
import { Link, withRouter } from "react-router-dom";
import superlogin from "superlogin-client";
import { SuperLoginClientConfig } from "../../../sl-client-config";
import {
  Form,
  FormGroup,
  Container,
  Row,
  Col,
  Card,
  CardGroup,
  CardBody,
  Button
} from "reactstrap";
import {
  PasswordField,
  VerifiedPasswordField
} from "../../../components/Inputs/Auth";

superlogin.configure(SuperLoginClientConfig);

type Props = {
  match: Object,
  location: Object,
  history: Object,
  staticContext: Object
};

type State = {
  password: string,
  confirmPassword: string,
  token: string
};

class ResetPassword extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      password: "",
      confirmPassword: "",
      token: this.props.match.params.token
    };
    
    this.handleUserInput = this.handleUserInput.bind(this);
    
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleUserInput(e: SyntheticEvent<HTMLInputElement>) {
    (e.currentTarget: HTMLInputElement);
    e.persist();

    const name = e.target.name;

    const value = e.target.value;

    if (name === "confirmPassword") {
      if (value != this.state.password) {
        e.target.setCustomValidity("Passwords do not match");
      } else {
        e.target.setCustomValidity("");
      }
    }

    this.setState({ [name]: value });
  }

  handleFormSubmit(e: SyntheticEvent<HTMLButtonElement>) {
    e.preventDefault();
    // add errors for submitting using usernameRegex & passwordRegex setvalidity

    superlogin
      .resetPassword(this.state)
      .then(req => this.props.history.push("/login"))
      .catch(err => console.log("reset password on form submit", err.error));
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup>
                <Card className="p-4">
                  <Form onSubmit={this.handleFormSubmit}>
                    <CardBody className="p-4">
                      <h1 className="text-center">Reset Password</h1>
                      <p className="text-muted">Type in your new password</p>
                      <FormGroup>
                        <PasswordField
                          value={this.state.password}
                          onChangeValue={this.handleUserInput}
                        />
                      </FormGroup>
                      <FormGroup>
                        <VerifiedPasswordField
                          value={this.state.confirmPassword}
                          onChangeValue={this.handleUserInput}
                        />
                      </FormGroup>
                      <Row>
                        <Col md={{ size: 4, offset: 4 }} xs="12">
                          <Button
                            color="success"
                            type="submit"
                            value="Submit"
                            block
                          >
                            Update Password
                          </Button>
                        </Col>
                      </Row>
                    </CardBody>
                  </Form>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default ResetPassword;
