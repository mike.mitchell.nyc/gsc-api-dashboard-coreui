

import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Form,
  FormGroup,
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Input,
  InputGroup,
  InputGroupAddon,
  FormText,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  ListGroup,
  ListGroupItem
} from "reactstrap";
import * as _ from "lodash";
import { Props } from "../../../SharedTypes";

//axios
import axios from "axios";

// pouchdb
import PouchDB from "pouchdb";

type State = {
  modal: boolean,
  apiKeys: [],
  accept: string
};

type GoogleResponse = {
  profileObj: {
    email: string,
    familyName: string,
    givenName: string,
    googleId: string,
    imageUrl: string,
    name: string
  },
  googleId: string,
  tokenObj: {
    access_token: string,
    expires_at: number,
    expires_in: number,
    first_issued_at: number,
    id_token: string,
    idpId: string,
    login_hint: string,
    scope: string,
    session_state: {
      extraQueryParams: {
        authuser: string
      }
    },
    token_type: string
  }
};

export class ApiSettings extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      modal: false,
      apiKeys: [],
      accept: ""
    };

    (this: any).toggle = this.toggle.bind(this);

    (this: any).removeApiKey = this.removeApiKey.bind(this);

    (this: any).onSubmit = this.onSubmit.bind(this);
  }

  async componentWillMount() {
    console.log(this);
    const remoteDB = await new PouchDB(this.props.remoteDB);
    const localDB = await new PouchDB(this.props.localDB);
    const keys = await remoteDB
      .get("keys")
      .then(keys => {
        return keys;
      })
      .catch(e => console.log(e));
    const keyEmails = keys.keys.map(key => {
      return { email: key.metadata.email };
    });
    console.log(keyEmails);
    this.setState({ apiKeys: keyEmails }, () => console.log(this.state));
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  removeApiKey(e: SyntheticEvent<HTMLInputElement>) {
    (e.currentTarget: HTMLInputElement);
    e.preventDefault();
  }

  onSubmit(e: SyntheticEvent<HTMLInputElement>) {
    (e.currentTarget: HTMLInputElement);
    e.preventDefault();

    googlePopup(this.props);
  }

  render() {
    return (
      <div style={{ maxHeight: "100%" }}>
        <Card className="mx-4" style={{ minWidth: "100%" }}>
          <Form>
            <CardBody className="p-4">
              <h1>API Credentials</h1>
              <p className="muted-text">Google Search Console API Accounts</p>
              <Table>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Email</th>
                    <th>Date Added</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                  </tr>
                </tbody>
              </Table>

              <Row className="mb-2">
                <Col>
                  <Button onClick={this.onSubmit.bind(this)}>
                    Add Google Search Console API
                  </Button>
                </Col>
              </Row>
              <Row>
                <Button color="link" onClick={this.toggle}>
                  How do I enable my GSC API?
                </Button>
              </Row>
              <Modal
                isOpen={this.state.modal}
                toggle={this.toggle}
                className={this.props.className}
                size="lg"
                style={{ minWidth: "65%" }}
              >
                <ModalHeader toggle={this.toggle}>
                  How to Add Your Google API Key
                </ModalHeader>
                <ModalBody>
                  <ListGroup>
                    <ol>
                      <li>
                        <ListGroupItem style={{ border: 0 }} listItem={false}>
                          Use{" "}
                          <a
                            target="_blank"
                            href="https://console.developers.google.com/start/api?id=webmasters"
                            rel="noopener"
                          >
                            this wizard
                          </a>{" "}
                          to create or select a project in the Google Developers
                          Console and automatically turn on the API.
                        </ListGroupItem>
                      </li>
                      <li>
                        <ListGroupItem style={{ border: 0 }}>
                          Click Continue, then go to Credentials.
                        </ListGroupItem>
                      </li>
                      <li>
                        <ListGroupItem style={{ border: 0 }}>
                          On the Add Credentials To Your Project page, click the
                          Cancel button.
                        </ListGroupItem>
                      </li>
                      <li>
                        <ListGroupItem style={{ border: 0 }}>
                          At the top of the page, select the OAuth consent
                          screen tab. Select an Email address, enter a Product
                          name if not already set, and click the Save button.
                        </ListGroupItem>
                      </li>
                      <li>
                        <ListGroupItem style={{ border: 0 }}>
                          Select the Credentials tab, click the Create
                          credentials button and select OAuth client ID.
                        </ListGroupItem>
                      </li>
                      <li>
                        <ListGroupItem style={{ border: 0 }}>
                          Select the application type <strong>Other</strong>,
                          enter the name "GSC Advanced", and click the Create
                          button.
                        </ListGroupItem>
                      </li>
                      <li>
                        <ListGroupItem style={{ border: 0 }}>
                          Click OK to dismiss the resulting dialog.
                        </ListGroupItem>
                      </li>
                      <li>
                        <ListGroupItem style={{ border: 0 }}>
                          Click the file_download (Download JSON) button to the
                          right of the client ID.
                        </ListGroupItem>
                      </li>
                      <li>
                        <ListGroupItem style={{ border: 0 }}>
                          Upload the file to Go Form Yourself
                        </ListGroupItem>
                      </li>
                      <li>
                        <ListGroupItem style={{ border: 0 }}>
                          client_secret.json files have this format:
                          <pre>
                            <code>
                              {`
{
  "installed": {
    "client_id": "33511313335151351-asdf98saf13nadflj1.apps.googleusercontent.com",
    "project_id": "GSC Advanced Example",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_secret": "jdsaf9po13kandf_13AD",
    "redirect_uris": [
      "urn:ietf:wg:oauth:2.0:oob",
      "http://localhost"
    ]
  }
}
`}{" "}
                            </code>
                          </pre>
                        </ListGroupItem>
                      </li>
                    </ol>
                  </ListGroup>
                </ModalBody>
              </Modal>

              <Col xs={4}>
                <Button color="danger">Remove API Key</Button>
              </Col>
            </CardBody>
          </Form>
        </Card>
      </div>
    );
  }
}

const googlePopup = props => {
  let width = 400;
  let height = 450;
  let left = screen.width / 2 - width / 2;
  let top = screen.height / 2 - height / 2;

  let googleLoginPopup = window.open(
    "",
    "_blank",
    `menubar=1,resizable=1,width=${width},height=${height},top=${top},left=${left}`
  );
  googleLoginPopup.document.write("Loading preview...");
  const URL = require("url-parse");
  const url = new URL(props.remoteDB);
  axios
    .post("http://localhost:5000/google-login", url, {
      headers: {
        Authorization: `Bearer ${url.username}:${url.password}`
      }
    })
    .then(response => {
      console.log(response.data);
      googleLoginPopup.location.href = response.data;
    })
    .catch(e => console.log(e));
};
