import React, { SyntheticEvent } from "react";
import { Link } from "react-router-dom";
import {
  Form,
  FormGroup,
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Input,
  InputGroup,
  InputGroupAddon,
  FormText,
  Button,
  Table
} from "reactstrap";

export const UserSettings = props => {
  return (
    <div style={{ maxHeight: "100%" }}>
      <Card style={{ minWidth: "100%" }}>
        <Form>
          <CardBody>
            <h1>User Settings</h1>
            <p>
              {props.metadata.firstName} {props.metadata.lastName}
            </p>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon>
                  <span className="input-group-text">
                    <i className="icon-user" />
                  </span>
                </InputGroupAddon>
                <div
                  className="d-flex align-items-center"
                  style={{
                    border: "1px solid #c2cfd6",
                    width: "100%",
                    paddingLeft: 12.5
                  }}
                >
                  {props.metadata.username}
                </div>
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon>
                  <span className="input-group-text">
                    <span className="input-group-text">@</span>
                  </span>
                </InputGroupAddon>
                <div
                  className="d-flex align-items-center"
                  style={{
                    border: "1px solid #c2cfd6",
                    width: "100%",
                    paddingLeft: 12.5
                  }}
                >
                  {props.metadata.email}
                </div>
              </InputGroup>
            </FormGroup>

            <Row className="mb-2">
              <Col>
                <Button color="success" type="submit" value="Submit">
                  Update Email Address
                </Button>
              </Col>
              <Col>
                <Link to="/change-password">
                  <Button color="info">
                    <span>Change Password</span>
                  </Button>
                </Link>
              </Col>
            </Row>
            <Row />
          </CardBody>
        </Form>
      </Card>
    </div>
  );
};
