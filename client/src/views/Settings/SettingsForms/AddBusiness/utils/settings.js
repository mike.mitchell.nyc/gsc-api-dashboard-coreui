export const formInfo = {
  formTitle: "Add Your Business",
  formInstructionsCSS: "text-danger font-weight-bold",
  formInstructions:
    "Warning: Incorrect or inconsistent information will likely negatively affect your rankings.",
  submitButtonText: "Add Business"
};

export const formState = {
  businessName: "",
  addressLineOne: "",
  addressLineTwo: "",
  city: "",
  state: "",
  zipcode: "",
  country: "",
  phone: "",
  website: "",
  businessCategories: "",
  openHours: [],
  shortestDescription: "",
  shortDescription: "",
  normalDescription: "",
  longDescription: "",
  periods: undefined,
  open_now: undefined,
  weekday_hours: "",
  dateInputsToggle: false,
  mondayOpen: "",
  mondayClosed: "",
  tuesdayOpen: "",
  tuesdayClosed: "",
  wednesdayOpen: "",
  wednesdayClosed: "",
  thursdayOpen: "",
  thursdayClosed: "",
  fridayOpen: "",
  fridayClosed: "",
  saturdayOpen: "",
  saturdayClosed: "",
  sundayOpen: "",
  sundayClosed: ""
};

export const businessInputs = [
  {
    type: "text",
    id: "businessName",
    cssClass: "",
    label: { text: "Business Name" },
    placeholder: "Best Company USA",
    required: true,
    pattern: null,
    title: null,
    helpText: "Add Your Business Name",
    icon: null
  },
  {
    type: "text",
    id: "addressLineOne",
    cssClass: "",
    placeholder: "123 Mulberry Street",
    required: true,
    pattern: null,
    title: null,
    helpText: "Make sure all address information is accurate",
    label: { text: "Address Line 1" }
  },
  {
    type: "text",
    id: "addressLineTwo",
    cssClass: "",
    placeholder: "Apartment, suite, unit, building, floor, etc.",
    required: false,
    pattern: null,
    title: null,
    label: { text: "Address Line 2" }
  },
  {
    type: "text",
    id: "city",
    cssClass: "",
    placeholder: "Springfield",
    required: true,
    pattern: null,
    title: null,
    helpText: "",
    label: { text: "City" }
  },
  {
    type: "text",
    id: "state",
    cssClass: "",
    placeholder: "New Jersey",
    label: { text: "State" },
    required: true,
    pattern: null,
    title: null
  },
  {
    type: "text",
    id: "zipcode",
    cssClass: "",
    placeholder: "12345 or 12345-6789",
    required: true,
    pattern: null,
    title: null,
    helpText: "",
    label: { text: "Zip Code" }
  },
  {
    type: "text",
    id: "country",
    cssClass: "",
    placeholder: "United States",
    required: true,
    pattern: null,
    title: null,
    helpText: "",
    label: { text: "Country" }
  },
  {
    type: "text",
    id: "phone",
    cssClass: "",
    placeholder: "555-555-5555",
    label: { text: "Phone Number" },
    required: true,
    pattern: null,
    title: null
  },
  {
    type: "text",
    id: "website",
    cssClass: "",
    placeholder: "https://mysite.com or https://mysite.com/second-location",
    required: true,
    pattern: null,
    title: null,
    helpText: "",
    label: { text: "Location Website" }
  },
  {
    type: "text",
    id: "businessCategories",
    cssClass: "",
    label: { text: "Business Categories" },
    placeholder: "Car Dealer, Antique Shop, etc.",
    required: true,
    pattern: null,
    title: null,
    helpText: "Add your categories seperated by commas",
    icon: null
  }
];

export const dateInputs = [
  {
    type: "time",
    id: "monday",
    cssClass: "",
    placeholder: "",
    label: { text: "Monday" },
    required: true,
    helpText: "Add Your Business Name",
    icon: null
  },
  {
    type: "time",
    id: "tuesday",
    cssClass: "",
    placeholder: "",
    required: true,
    pattern: null,
    title: null,
    helpText: "",
    label: { text: "Tuesday" }
  },
  {
    type: "time",
    id: "wednesday",
    cssClass: "",
    placeholder: "",
    required: false,
    pattern: null,
    title: null,
    label: { text: "Wednesday" }
  },
  {
    type: "time",
    id: "thursday",
    cssClass: "",
    placeholder: "Thursday",
    required: true,
    pattern: null,
    title: null,
    helpText: "",
    label: { text: "Thursday" }
  },
  {
    type: "time",
    id: "friday",
    cssClass: "",
    placeholder: "",
    label: { text: "Friday" },
    required: true,
    pattern: null,
    title: null
  },
  {
    type: "time",
    id: "saturday",
    cssClass: "",
    placeholder: "",
    required: true,
    pattern: null,
    title: null,
    helpText: "",
    label: { text: "Saturday" }
  },
  {
    type: "time",
    id: "sunday",
    cssClass: "",
    placeholder: "",
    required: true,
    pattern: null,
    title: null,
    helpText: "",
    label: { text: "Sunday" }
  }
];
