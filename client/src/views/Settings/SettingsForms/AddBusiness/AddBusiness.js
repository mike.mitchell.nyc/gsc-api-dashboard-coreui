import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Form,
  FormGroup,
  Row,
  Col,
  Card,
  CardGroup,
  CardBody,
  CardFooter,
  Button
} from "reactstrap";
import { FormField } from "../../../../components/FormField/FormField";

class AddBusiness extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props.formState
    };

    this.handleUserInput = this.handleUserInput.bind(this);

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleUserInput(e) {
    const name = e.target.name;

    const value = e.target.value;

    console.log(value, name);

    this.setState({ [name]: value }, () => console.log(this.state));
  }

  handleFormSubmit(e) {
    e.persist();
    e.preventDefault();
    console.log(this.state);
  }

  render() {
    console.log(this.props.inputs);
    return (
      <Card>
        <Form onSubmit={this.handleFormSubmit}>
          <CardBody>
            <h1>Add Your Business</h1>
            <p className="text-danger">
              <strong>
                Warning: Incorrect or inconsistent information will likely
                negatively affect your rankings.
              </strong>
            </p>
            <FormGroup>
              {this.props.inputs.map((customProps, item) => (
                <FormField
                  key={item}
                  {...customProps}
                  value={this.state[customProps.id]}
                  onChangeValue={this.handleUserInput}
                />
              ))}
            </FormGroup>

            <Row>
              <Col md={{ size: 4, offset: 4 }} xs="12">
                <Button color="success" type="submit" value="Submit" block>
                  Add Business
                </Button>
              </Col>
            </Row>
          </CardBody>
        </Form>
      </Card>
    );
  }
}

export default AddBusiness;
