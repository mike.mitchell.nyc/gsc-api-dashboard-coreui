import React, { Component, SyntheticEvent } from "react";
import { ApiSettings } from "./SettingsForms/ApiSettings";
import { UserSettings } from "./SettingsForms/UserSettings";
import { CitationManagerSettings } from "./SettingsForms/CitationManagerSettings";
import {
  businessInputs,
  formState,
  formInfo,
  dateInputs
} from "./SettingsForms/AddBusiness/utils/settings";
import TightForm from "../../components/Forms/TightForm/TightForm";

import InlineForm from "../../components/Forms/InlineForm/InlineForm";

import {
  Form,
  FormGroup,
  Row,
  Col,
  Card,
  CardGroup,
  CardBody,
  CardFooter,
  Button,
  Label,
  Input
} from "reactstrap";

const Settings = props => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col className="mx-auto" sm="12" md="8">
          <InlineForm
            formInfo={{ ...formInfo }}
            formState={{ ...formState }}
            businessInputs={businessInputs}
            dateInputs={dateInputs}
            {...props}
          />
        </Col>

        <br />

        <Col className="mx-auto" sm="12" md="8">
          <UserSettings {...props} />
        </Col>
      </Row>
    </div>
  );
};

export default Settings;
