import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import PouchDB from "pouchdb";
import TypeaheadInput from "../../components/GSCAdvancedDashboard/TypeaheadInput/TypeaheadInput";
import GSCAdvancedTable from "../../components/GSCAdvancedDashboard/GSCAdvancedTable/GSCAdvancedTable";
import { decimalAdjust } from "../../components/UsefulFunctions/DecimalAdjust/DecimalAdjust";
import InfoPanel from "../../components/GSCAdvancedDashboard/InfoPanel/InfoPanel";
import BarChart from "../../components/GSCAdvancedDashboard/BarChart/BarChart";
import { gscDataGenerator } from "../../components/UsefulFunctions/GSCDataGenerator/GSCDataGenerator";
// lodash
import _ from "lodash";
const url = require("url");

// Decimal round
const round10 = function(value, exp) {
  return decimalAdjust("round", value, exp);
};

class GSCAdvanced extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      targets: [],
      clicks: {},
      impressions: {},
      ctr: {},
      position: {},
      xAxesLabels: [],
      isLoaded: false,
      summedClicks: 0,
      summedImpressions: 0,
      averageCTR: 0,
      averagePosition: 0,
      totalKeywords: 0,
      pageCount: 0,
      topThreePosition: 0,
      oneToFiftyPosition: 0,
      elevenToFiftyPosition: 0,
      threeToTenPosition: 0,
      siteCount: 0,
      siteList: [],
      sitesToFilter: [],
      originalData: [],
      tooltipOpen: false
    };

    (this).typeaheadFilter = this.typeaheadFilter.bind(this);
    (this).groupedByCharacteristic = this.groupedByCharacteristic.bind(
      this
    );
    (this).tooltipToggle = this.tooltipToggle.bind(this);
  }

  tooltipToggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  groupedByCharacteristic(
    key,
    dataToTransform: [] = this.state.originalData
  ) {
    return _([...dataToTransform])
      .groupBy(key || "keyword")
      .map(dataCombined => {
        const positionTotal = dataCombined.reduce(
          (accumulator, currentValue, currentIndex, array) => {
            return (accumulator += currentValue.position);
          },
          0
        );

        const clickTotal = dataCombined.reduce(
          (accumulator, currentValue, currentIndex, array) => {
            return (accumulator += currentValue.clicks);
          },
          0
        );

        const ctrTotal = dataCombined.reduce(
          (accumulator, currentValue, currentIndex, array) => {
            return (accumulator += currentValue.ctr);
          },
          0
        );

        const impressionsTotal = dataCombined.reduce(
          (accumulator, currentValue, currentIndex, array) => {
            return (accumulator += currentValue.impressions);
          },
          0
        );

        const uniqueSites = [...new Set(dataCombined.map(item => item.site))];
        const uniquePages = [...new Set(dataCombined.map(item => item.page))];
        const uniquePaths = [...new Set(dataCombined.map(item => item.path))];

        const id = (
          "#" +
          dataCombined[0].keyword +
          impressionsTotal +
          ctrTotal +
          clickTotal +
          positionTotal
        ).replace(/[\W_]+/g, "");

        const tableData = {
          clicks: clickTotal,
          ctr: round10(ctrTotal / dataCombined.length, -2),
          date: dataCombined.length,
          impressions: impressionsTotal,
          keyword: dataCombined[0].keyword,
          page: uniquePages.length,
          path:
            uniquePaths.length < 2 && uniquePaths[0] === "/" ? (
              <div id={id}>Homepage</div>
            ) : uniquePaths.length < 2 ? (
              uniquePaths
            ) : uniquePaths[0] === "/" ? (
              `Homepage and ${uniquePaths.length - 1} More`
            ) : (
              `${uniquePaths[0]} and ${uniquePaths.length - 1} More`
            ),
          position: round10(positionTotal / dataCombined.length, -2),
          site:
            uniqueSites.length < 2
              ? uniqueSites
              : [`${uniqueSites[0]} and ${uniqueSites.length - 1} More`],
          uniqueSites,
          uniquePages,
          uniquePaths,
          id
        };

        /* console.log(tableData);
        {
          clicks: 0;
          ctr: 0;
          date: 2;
          impressions: 3;
          keyword: "schema org generator";
          page: 1;
          path: ["/tools/schema-org-generator/"];
          position: 90.75;
          site: ["https://goformyourself.com/"];
          uniquePages: [
            "https://goformyourself.com/tools/schema-org-generator/"
          ];
          uniquePaths: ["/tools/schema-org-generator/"];
          __proto__: Object;
        } */
        return tableData;
      })
      .value();
    //  console.log(groupedByCharacteristic());
  }

  async componentWillMount() {
    const remoteDB = await new PouchDB(this.props.remoteDB);
    const localDB = await new PouchDB(this.props.localDB);
    console.log(this.props, "this.props");

    await localDB
      .allDocs({
        include_docs: true,
        attachments: true,
        startkey: "pq",
        endkey: "pq\ufff0"
      })
      .then(result => {
        let newState = gscDataGenerator(result.rows);

        newState.data = this.groupedByCharacteristic(
          "keyword",

          newState.originalData
        );

        this.setState({ ...newState });
      })
      .catch(function(err) {
        console.log(err);
      });
  }

  typeaheadFilter(sitesSubmitted) {
    let sites = [...this.state.sitesToFilter];
    sites.push(sitesSubmitted);
    const flattenedSites = _.flatten(sites);
    const uniqueSites = _.uniq(flattenedSites);
    const oldData = [...this.groupedByCharacteristic("keyword")];

    const newData = oldData.filter(data => {
      let status = false;
      data.site.forEach(site => {
        if (uniqueSites.indexOf(site) >= 0) {
          status = true;
        }
      });
      return status;
    });

    this.setState({ sitesToFilter: sitesSubmitted, data: newData });

    // this.setState(
    //   {
    //     data: data
    //   },
    //   () => console.log(this.state.data)
    // );
  }

  render() {
    return (
      <div className="animated fadeIn">
        {this.state.isLoaded && (
          <div>
            <TypeaheadInput
              data={this.state.siteList}
              filterData={this.typeaheadFilter}
            />
            {/* $FlowFixMe*/}
            <InfoPanel data={this.state} />

            <BarChart data={this.state} />

            <Row className="justify-content-center">
              <Col md="10">
                <GSCAdvancedTable
                  {...{ ...this.props, ...this.state }}
                  tooltipToggle={this.tooltipToggle}
                />
              </Col>
            </Row>
          </div>
        )}
      </div>
    );
  }
}

export default GSCAdvanced;
