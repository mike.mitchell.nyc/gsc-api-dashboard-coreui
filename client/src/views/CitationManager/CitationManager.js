import React, { Component } from "react";
import { Collapse, Button, CardBody, Card, Row } from "reactstrap";
import CitationList from "../../components/CitationManager/CitationList";
import Accordion from "../../components/Accordion/Accordion";
import PouchDB from "pouchdb";

class CitationManager extends Component {
  constructor(props) {
    super(props);

    this.state = {
      businesses: ""
    };
  }

  async componentWillMount() {
    console.log(this.props);

    const localDB = await new PouchDB(this.props.localDB);
    const remoteDB = await new PouchDB(this.props.remoteDB);

    localDB.sync(remoteDB);
    await localDB
      .allDocs({
        include_docs: true,
        startkey: "cc_",
        endkey: "cc_\ufff0"
      })
      .then(result => {
        this.setState({ businesses: result.rows }, () =>
          console.log(this.state)
        );
      })
      .catch(function(err) {
        console.log(err);
      });
  }

  // toggle() {
  //   this.setState({ collapse: !this.state.collapse });
  // }

  render() {
    return <div>{/*<CitationList />*/}</div>;
  }
}

export default CitationManager;
