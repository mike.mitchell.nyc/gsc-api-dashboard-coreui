const createOrSyncPouchDB = async (metadata: Metadata) => {
  const remoteDB = await new PouchDB(metadata.userDBs.privatedb);
  const localDB = await new PouchDB(metadata.user_id);

  this.setState({
    remoteDB: metadata.userDBs.privatedb,
    localDB: metadata.user_id
  });

  let remoteMetadata: Metadata, localMetadata: Metadata;

  try {
    localMetadata = await localDB.get("metadata");
    remoteMetadata = await remoteDB.get("metadata");
  } catch (err) {
    if (err.status === 404) {
      try {
        remoteMetadata = await remoteDB.get("metadata");
        remoteDB.replicate
          .to(localDB, {
            filter: function(doc) {
              return doc._id === "metadata";
            }
          })
          .on("complete", async function() {
            localMetadata = await localDB.get("metadata");
          })
          .on("error", function(err) {
            console.log("sync not working", err);
            // this.props.history.push("/login");
          });
      } catch (err) {
        if (err.status === 404) {
          this.props.history.push("/login");
        }
      }
    }
  }

  if (_.isEqual(localMetadata, remoteMetadata)) {
    
    this.setState({
      isLoading: false,
      metadata: localMetadata,
      authorized: true
    });
  } else {
    remoteDB.sync(localDB);
    if (_.isEqual(localMetadata, remoteMetadata)) {
      
      this.setState(
        {
          isLoading: false,
          metadata: localMetadata,
          authorized: true
        },
        () => {
          remoteDB
            .close()
            .then(console.log("remote db closed"))
            .catch(console.log("remoteDB close error"));
          localDB
            .close()
            .then(console.log("local db closed"))
            .catch(console.log("local DB close error"));
        }
      );
    } else {
      this.props.history.push("/login");
    }
  }
};

export default createOrSyncPouchDB;
