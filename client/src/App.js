import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";


// Containers
import Full from "./containers/Full/";

// Views
import Login from "./views/Pages/Login/";
import Register from "./views/Pages/Register/";
import Page404 from "./views/Pages/Page404/";
import Page500 from "./views/Pages/Page500/";
import ForgotPassword from "./views/Pages/ForgotPassword/";
import ResetPassword from "./views/Pages/ResetPassword/";
import CloseWindow from "./views/Pages/CloseWindow/CloseWindow";

const App = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/login" name="Login Page" component={Login} />
            <Route
                exact
                path="/register"
                name="Register Page"
                component={Register}
            />
            <Route
                exact
                path="/close"
                name="Close Window"
                component={CloseWindow}
            />
            <Route exact path="/404" name="Page 404" component={Page404} />
            <Route exact path="/500" name="Page 500" component={Page500} />
            <Route
                exact
                path="/forgot-password"
                name="ForgotPassword"
                component={ForgotPassword}
            />
            <Route
                exact
                path="/password-reset/:token"
                name="ResetPassword"
                component={ResetPassword}
            />
            <Route path="/" name="Home" component={Full} />
        </Switch>
    </BrowserRouter>
);

export default App;
