### ReactJS, CouchDB, and PouchDB admin template with authorization

A website and user system starter using CoreUI admin template with CouchDB.

This project is built with NodeJS 9.5.0, [CoreUI React Full Project](https://github.com/mrholek/CoreUI-React/tree/master/React_Full_Project) (a Bootsrap 4 admin template made with [Create React App](https://github.com/facebookincubator/create-react-app)), [superlogin](https://github.com/colinskow/superlogin) for authentication with CouchDB and Redis, [superlogin-client](https://github.com/micky2be/superlogin-client) for login handling on the frontend, [PouchDB](https://github.com/pouchdb/pouchdb) for offline data storage , and [CouchDB](http://couchdb.apache.org/)as the database.

## Pre-requisites for Development
1. [NodeJS >= 8](https://nodejs.org/en/download/package-manager/)
2. [CouchDB > v2.0](http://couchdb.apache.org/) or [Cloudant account](https://www.ibm.com/cloud/cloudant)
3. [Redis](https://redis.io/)

## For Production
 

### Installation
1. `git clone repo`
2. Create .env file and add CouchDB username `DB_USER` and password `DB_PASS`
3. If using redis and are using a password update the .env file `REDIS_PASS` variable
4. If not using redis, update the superlogin config at `/sl-config.js` to use the superlogin memory adapter and remove the redis limiter code



 

Authentication settings for superlogin in Express are set in `/sl-config.js` while the settings for superlogin-client can be found at ./client/