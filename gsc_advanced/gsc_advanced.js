// Utility libraries and wrappers
const prom = require("nano-promises");
const util = require("util");
const dotenv = require("dotenv").config({ path: "../.env" });
const _ = require("lodash");

// CouchDB adapter
const nano = require("nano")(
  `http://${process.env.DB_USER}:${process.env.DB_PASS}@localhost:5984`
);

const PouchDB = require("pouchdb");

// get data from last 100 days
const { DateTime } = require("luxon");
const days = 100;
let dateArray = [];
for (let i = 0; i < days; i++) {
  dateArray.push(
    DateTime.local()
      .plus({ days: -i })
      .toISODate()
  );
}

const Bottleneck = require("bottleneck");
const { URL } = require("url");

// Never more than 1 request running at a time.
// Wait at least 75ms between each request.
const limiter = new Bottleneck({
  minTime: 300
});

/**
 *  All Google All The Time!
 */
const { google } = require("googleapis");
const { OAuth2Client } = require("google-auth-library");
const OAuth2 = google.auth.OAuth2;

/**
 *  Promisify webmasters and its methods
 */
const webmasters = google.webmasters("v3");
const gscList = util.promisify(webmasters.sites.list);
const gscQueryPromise = util.promisify(webmasters.searchanalytics.query);
const gscQuery = limiter.wrap(gscQueryPromise);

/**
 *  Client information
 */
const clientSecrets = require("./client_secret.json");
const CLIENT_ID = clientSecrets.web.client_id;
const CLIENT_SECRET = clientSecrets.web.client_secret;
const REDIRECT_URL = clientSecrets.web.redirect_uris[0];
const oauth2Client = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);

/**
 *  Wrap Nano in a promise
 */
const allDBs = prom(nano);

console.log("working");
// list all DBs in array -> stored in body is an array
allDBs.db.list(async function(err, body) {
  /**
   * Array filters items based on search criteria (query)
   */

  // filter items to only dbs that start with privatedb$ and return array
  const userDBs = filterItems("privatedb$", body);

  // db is the text name of the db
  userDBs.forEach(async function(dbName) {
    // userDB is for all DBs
    const userDB = await allDBs.use(dbName);

    // this gets the keys document
    const keys = await userDB
      .get("keys")
      // keys[0] is the return object
      .then(keysDocumentResponse => {
        console.log(keysDocumentResponse);
        // get the json of the document
        const keysDocument = keysDocumentResponse[0];
        const keysArray = keysDocument.keys;
        let urlList = [];

        // get the tokens and start the process for each key
        keysArray.forEach(async key => {
          // this is a tokens array
          const tokensArray = key.tokens;
          const email = key.metadata.email;

          // last token in the array
          const mostRecentToken =
            tokensArray[tokensArray.length - 1] || tokensArray[0];
          console.log(mostRecentToken.refresh_token);

          // set the credentials using the most recent token
          oauth2Client.setCredentials({
            access_token: mostRecentToken.access_token,
            refresh_token: mostRecentToken.refresh_token,
            expiry_date: mostRecentToken.expiry_date
          });

          // if the token has expired then get another one
          if (mostRecentToken.expiry_date < Date.now()) {
            // refresh access token
            oauth2Client.refreshAccessToken(function(err, tokens) {
              if (err) {
                console.log("err", err);
              }

              // add tokens to the tokens array
              tokensArray.push(tokens);

              console.log("keysdocument", keysDocument);

              // add that into the keys db
              userDB
                .insert(keysDocument, "keys")
                .then(function([body, headers]) {
                  console.log("successfully inserted tokens", body, headers);
                })
                .catch(function(err) {
                  console.error("error inserting tokens: ", err);
                });
            });
          }

          let bulkDocsArray = [];
          let errorList = [];
          const db = new PouchDB(
            `http://${process.env.DB_USER}:${
              process.env.DB_PASS
            }@localhost:5984/${dbName}`
          );

          // get a list of all the documents in the pouchdb so that we don't duplicate a document
          const docList = await db
            .allDocs()
            .then(response => response.rows.map(row => row.id))
            .catch(console.log);

          const gscQueryErrorLog = await db
            .get("gscErrors")
            .then(errorDocumentResponse => errorDocumentResponse)
            .catch(err => console.log("error retrieiving queryErrorLog", err));

          const loggedErrors = await gscQueryErrorLog.noData;

          const loggedErrorsLength = loggedErrors.length;

          // query the webmasters scope for sites using oauth2Client as the auth and the maximum number of sites
          const sites = await gscList({
            auth: oauth2Client,
            rowLimit: 1000
          })
            .then(
              // get each site
              function(response) {
                // get the list of URLs from the response at data.siteEntry

                const urlList = response.data.siteEntry;
                if (urlList.length === 0) {
                  return console.log("no urls found");
                }
                return urlList.map(f => {
                  return f.siteUrl;
                });
              }
            )
            .catch(e => console.log("listing sites error", e));

          let queryCount = days * sites.length;

          let count = 0;

          // get each site
          sites.forEach(site => {
            // strip out protocol and trailing slash

            // encode url for transmitting over https to google
            const siteUrlEncoded = encodeURIComponent(site);

            // Get all date ranges and construct the query

            dateArray.forEach(async date => {
              // get start and end dates correctly assigned

              // construct the query
              const query = {
                auth: oauth2Client,
                siteUrl: siteUrlEncoded,
                resource: {
                  startDate: date,
                  endDate: date,
                  dimensions: ["page", "query"],
                  // searchType: default is 'web'
                  rowLimit: 5000,
                  aggregationType: "byPage"
                }
              };

              // takes the query, returns the first letter of each result, sorts alphabetically and then joins them as a string
              // the first unique paramter in the docname
              const queryType = query.resource.dimensions
                .map(query => query.match(/\b\w/g))
                .sort()
                .join("");

              // build the document name
              const docName = `${queryType}_${site}_${date}`;

              // check to see if doc is created or if it has already been identified as lacking data & skip it to avoid even thinking about it
              flattenedErrors = _.flatten(loggedErrors);
              if (
                docList.includes(docName) ||
                flattenedErrors.includes(docName)
              ) {
                --queryCount;
              } else {
                // create the documents
                await gscQuery(query)
                  .then(response => {
                    count++;
                    const rows = response.data.rows;
                    if (rows) {
                      // if rows is not undefined create a document
                      const modifiedRows = rows.map(row => {
                        const rankingPage = new URL(row.keys[0]);
                        const urlPath = rankingPage.pathname;
                        let page = row.keys[0];
                        let keyword = row.keys[1];
                        row.keyword = keyword;
                        row.page = page;
                        row.site = site;
                        row.path = urlPath;
                        delete row.keys;
                        row.date = date;
                        return row;
                      });
                      bulkDocsArray.push({
                        _id: docName,
                        type: queryType,
                        date,
                        email,
                        site,
                        modifiedRows
                      });
                    } else {
                      // note that there is no data for this document
                      console.log("no data for ", docName, date);
                      if (
                        DateTime.local().plus({ days: -3 }) >
                        DateTime.fromISO(date)
                      ) {
                        loggedErrors.push([docName, date, site]);
                        console.log(site + " undefined on this day " + date);
                      }
                    }
                  })
                  .catch(e => {
                    count++;
                    errorList.push({ e, site });

                    console.log(
                      "error for real from trying to get rows",
                      site,
                      e
                    );
                  });
                console.log(queryCount, count);
                if (queryCount === count) {
                  console.log(
                    "bulkdocsarray",
                    bulkDocsArray.length,
                    errorList.length,
                    gscQueryErrorLog.noData.length - loggedErrorsLength
                  );

                  bulkDocsArray.push(gscQueryErrorLog);
                  // add all the docs in and update the metadata
                  db
                    .bulkDocs(bulkDocsArray)
                    .then(function(result) {
                      console.log("bulkdoc put success", result);
                    })
                    .catch(function(err) {
                      console.log("failed to put bulk docs", err);
                    });
                }
              }
            });
          });
        });
      })
      .catch(e => {
        if (e.error === "not_found") {
          // console.log("this user has no keys db", db);
        }
      });
  });
});

const filterItems = function(query, body) {
  return body.filter(function(el) {
    return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
  });
};

/* List of commands

webmasters.searchanalytics.query 
    Query your data with filters and parameters that you define. Returns zero or more rows grouped by the row keys that you define. You must define a date range of one or more days. When date is one of the group by values, any days without data are omitted from the result list. If you need to know which days have data, issue a broad date range query grouped by date for any metric, and see which day rows are returned.
webmasters.sitemaps.delete
    Deletes a sitemap from this site.
webmasters.sitemaps.get
    Retrieves information about a specific sitemap.
webmasters.sitemaps.list
	Lists the sitemaps-entries submitted for this site, or included in the sitemap index file (if sitemapIndex is specified in the request).
webmasters.sitemaps.submit
	Submits a sitemap for a site.
webmasters.sites.add
	Adds a site to the set of the user's sites in Search Console.
webmasters.sites.delete
	Removes a site from the set of the user's Search Console sites.
webmasters.sites.get
	Retrieves information about specific site.
webmasters.sites.list
	Lists the user's Search Console sites.
webmasters.urlcrawlerrorscounts.query
	Retrieves a time series of the number of URL crawl errors per error category and platform.
webmasters.urlcrawlerrorssamples.get
	Retrieves details about crawl errors for a site's sample URL.
webmasters.urlcrawlerrorssamples.list
	Lists a site's sample URLs for the specified crawl error category and platform.
webmasters.urlcrawlerrorssamples.markAsFixed	
    Marks the provided site's sample URL as fixed, and removes it from the samples list.

*/
