const { DateTime } = require("luxon");
const fs = require("fs");

const today = DateTime.local().toISODate();

const days = 100;

let dateArray = [];

for (let i = 0; i < days; i++) {
  dateArray.push(
    DateTime.local()
      .plus({ days: -i })
      .toISODate()
  );
}

// differenceArray is [startDate,endDate] for the webmasters API
// let differenceArray = [];

// push values into the differenceArray
// for (let i = 0; i < days; i++) {
//     for (j = 0; j < days; j++) {
//         differenceArray.push([
//             DateTime.local()
//                 .plus({ days: -j })
//                 .toISODate(),
//             DateTime.local()
//                 .plus({ days: -i })
//                 .toISODate()
//         ]);
//     }
// }

fs.writeFile(
  `ArrayDate${today}.js`,
  "module.exports = " + JSON.stringify(dateArray),
  err => {
    if (err) throw err;
    console.log("The file has been saved!");
  }
);
