const PouchDB = require("pouchdb");
const dotenv = require("dotenv").config({ path: "../.env" });
const _ = require("lodash");
const moment = require("moment");
const prom = require("nano-promises");
const nano = require("nano")(
  `http://${process.env.DB_USER}:${process.env.DB_PASS}@localhost:5984`
);

const bigDB = new PouchDB(
  `http://${process.env.DB_USER}:${process.env.DB_PASS}@localhost:5984/`
);

const allDBs = prom(nano);

// list all DBs in array -> stored in body is an array
allDBs.db.list(async function(err, body) {
  const userDBs = filterItems("privatedb$", body);
  userDBs.forEach(async dbName => {
    const db = new PouchDB(
      `http://${process.env.DB_USER}:${
        process.env.DB_PASS
      }@localhost:5984/${dbName}`
    );
    const docList = await db
      .allDocs({
        include_docs: true,
        attachments: true,
        startkey: "pq",
        endkey: "pq\ufff0"
      })
      .then(result => gscDataGenerator(result.rows))
      .catch(console.log);

    db
      .get("metadata")
      .then(doc => {
        doc.allClicks = docList.summedClicks;
        doc.allImpressions = docList.summedImpressions;
        doc.averagePosition = docList.averagePosition;
        doc.topThreePosition = docList.topThreePosition;
        doc.threeToTenPosition = docList.threeToTenPosition;
        doc.elevenToFiftyPosition = docList.elevenToFiftyPosition;
        doc.pageCount = docList.pageCount;
        doc.totalKeywords = docList.totalKeywords;
        doc.siteCount = docList.siteCount;
        doc.averageCTR = docList.averageCTR;
        doc.urls.push(...docList.siteList);
        doc.urls = _.uniq(doc.urls);
        return db.put({
          ...doc
        });
      })
      .catch(console.log);

    console.log(
      docList.summedClicks,
      docList.summedImpressions,
      docList.averagePosition,
      docList.topThreePosition,
      docList.threeToTenPosition,
      docList.elevenToFiftyPosition,
      docList.pageCount,
      docList.totalKeywords,
      docList.siteCount,
      docList.averageCTR
    );
  });
});

const gscDataGenerator = pouchData => {
  const flattenedResults = pouchData
    .map(row => row.doc.modifiedRows)
    .reduce(function(accumulator, currentValue) {
      return accumulator.concat(currentValue);
    }, []);

  const summedClicks = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) => {
      return accumulator + currentValue.clicks;
    },
    0
  );

  const summedImpressions = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) => {
      return accumulator + currentValue.impressions;
    },
    0
  );

  const summedPositions = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) => {
      return accumulator + currentValue.position;
    },
    0
  );

  const averagePosition = round10(
    summedPositions / flattenedResults.length * 10,
    -2
  );

  const summedCTR = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) => {
      return accumulator + currentValue.ctr;
    },
    0
  );

  const averageCTR = round10(summedCTR / flattenedResults.length * 10, -2);

  const topThreePosition = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) =>
      currentValue.position <= 3 ? (accumulator += 1) : accumulator,
    0
  );

  const threeToTenPosition = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) =>
      currentValue.position >= 4 && currentValue.position <= 10
        ? (accumulator += 1)
        : accumulator,
    0
  );

  const elevenToFiftyPosition = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) =>
      currentValue.position >= 10 && currentValue.position <= 50
        ? (accumulator += 1)
        : accumulator,
    0
  );

  const oneToFiftyPosition = flattenedResults.reduce(
    (accumulator, currentValue, currentIndex, array) =>
      currentValue.position <= 50 ? (accumulator += 1) : accumulator,
    0
  );

  const uniquePages = [...new Set(flattenedResults.map(item => item.page))];
  const siteList = [...new Set(flattenedResults.map(item => item.site))];

  const siteCount = siteList.length;
  const pageCount = uniquePages.length;

  const totalKeywords = Object.keys(
    _(flattenedResults)
      .groupBy("keyword")
      .value()
  ).length;

  // search the flattenedResults by date and create a new object based on the dates
  const groupedByDate = _(flattenedResults)
    .groupBy("date")
    .map(dateDataCombined => dateDataCombined)
    .value();

  // combine all data for dates as an array [date, clicks, impressions, ctr, position]
  const datesCombined = groupedByDate.map(dataCombined => {
    const clicksData = dataCombined.reduce(
      (accumulator, currentValue, currentIndex, array) => {
        return accumulator + currentValue.clicks;
      },
      0
    );

    const impressionsData = dataCombined.reduce(
      (accumulator, currentValue, currentIndex, array) => {
        return accumulator + currentValue.impressions;
      },
      0
    );

    const ctrData = dataCombined.reduce(
      (accumulator, currentValue, currentIndex, array) => {
        return accumulator + currentValue.ctr;
      },
      0
    );

    const positionData = dataCombined.reduce(
      (accumulator, currentValue, currentIndex, array) => {
        return accumulator + currentValue.position;
      },
      0
    );

    return [
      dataCombined[0].date,
      clicksData,
      impressionsData,
      round10(ctrData / dataCombined.length * 100, -2),
      round10(positionData / dataCombined.length, -2)
    ];
  });

  // create x,y values for chartjs for clicks, impressions, ctr, position

  const labels = datesCombined.map(data =>
    moment(data[0]).format("MMM DD, YYYY")
  );

  const clicksCombined = datesCombined.map(data => {
    return {
      t: moment(data[0]),
      y: data[1]
    };
  });

  const impressionsCombined = datesCombined.map(data => {
    return {
      t: moment(data[0]),
      y: data[2]
    };
  });

  const ctrCombined = datesCombined.map(data => {
    return {
      t: moment(data[0]),
      y: data[3]
    };
  });

  const positionsCombined = datesCombined.map(data => {
    return {
      t: moment(data[0]),
      y: data[4]
    };
  });

  return {
    clicks: clicksCombined,
    impressions: impressionsCombined,
    ctr: ctrCombined,
    position: positionsCombined,
    originalData: flattenedResults,
    xAxesLabels: labels,
    isLoaded: true,
    summedClicks,
    averageCTR,
    summedImpressions,
    averagePosition,
    topThreePosition,
    threeToTenPosition,
    elevenToFiftyPosition,
    pageCount,
    totalKeywords,
    siteList,
    siteCount,
    oneToFiftyPosition
  };
};

// allDBs.map(async db => {
//   await db
//     .allDocs({
//       include_docs: true,
//       attachments: true,
//       startkey: "pq",
//       endkey: "pq\ufff0"
//     })
//     .then(result => {
//       let newState = gscDataGenerator(result.rows);
//       console.log(newState);
//     })
//     .catch(function(err) {
//       console.log(err);
//     });
// });

const filterItems = function(query, body) {
  return body.filter(function(el) {
    return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
  });
};

// Decimal round
const round10 = function(value, exp) {
  return decimalAdjust("round", value, exp);
};

function decimalAdjust(type, value, exp) {
  // If the exp is undefined or zero...
  if (typeof exp === "undefined" || +exp === 0) {
    return Math[type](value);
  }
  value = +value;
  exp = +exp;
  // If the value is not a number or the exp is not an integer...
  if (
    value === null ||
    isNaN(value) ||
    !(typeof exp === "number" && exp % 1 === 0)
  ) {
    return NaN;
  }
  // Shift
  value = value.toString().split("e");
  value = Math[type](+(value[0] + "e" + (value[1] ? +value[1] - exp : -exp)));
  // Shift back
  value = value.toString().split("e");
  return +(value[0] + "e" + (value[1] ? +value[1] + exp : exp));
}
